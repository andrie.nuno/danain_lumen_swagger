<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware' => 'auth','prefix' => 'api'], function ($router) {
    $router->get('me', 'AuthController@me');

    // Dashboard Metrics
    $router->get('newuser', 'NewUserController@index');
    $router->get('usercount', 'UserCountController@index');
    $router->get('penyerapanindividu', 'PenyerapanIndividuController@index');
    $router->get('penyerapaniinstitusi', 'PenyerapanInstitusiController@index');
    $router->get('activeuser', 'ActiveUserController@index');
    // $router->post('activeuser', 'ActiveUserPostController@index');
});

$router->group(['prefix' => 'api'], function () use ($router) {
   $router->post('/register', 'AuthController@register');
   $router->post('/login', 'AuthController@login');
});

$router->post('/activeuser', 'ActiveUserPostController@index');

$router->get('/posts', 'PostsController@index');
$router->get('/posts2', 'GSheetSampleController@index');

// Data Body Pusdafil
$router->post('/ivbodypusdafil', 'BodyController@iv');

// Pusdafil Before Encrypt
$router->get('/regpengguna', 'RPenggunaController@index');
$router->get('/regborrower', 'RBorrowerController@index');
$router->get('/reglender', 'RLenderController@index');
$router->get('/pengajuanpinjaman', 'PengPinjamanController@index');
$router->get('/pengajuanpemberianpinjaman', 'PemPinjamanController@index');
$router->get('/transaksipinjammeminjam', 'TransaksiController@index');
$router->get('/pembayaranpinjaman', 'PembayaranController@index');

// Pusdafil Encrypt
$router->post('/encregpengguna', 'RPenggunaController@enc');
$router->post('/encregborrower', 'RBorrowerController@enc');
$router->post('/encreglender', 'RLenderController@enc');
$router->post('/encpengajuanpinjaman', 'PengPinjamanController@enc');
$router->post('/encpengajuanpemberianpinjaman', 'PemPinjamanController@enc');
$router->post('/enctransaksipinjammeminjam', 'TransaksiController@enc');
$router->post('/encpembayaranpinjaman', 'PembayaranController@enc');

// Pusdafil Encrypt Concat IV
$router->post('/ivregpengguna', 'RPenggunaController@iv');
$router->post('/ivregborrower', 'RBorrowerController@iv');
$router->post('/ivreglender', 'RLenderController@iv');
$router->post('/ivpengajuanpinjaman', 'PengPinjamanController@iv');
$router->post('/ivpengajuanpemberianpinjaman', 'PemPinjamanController@iv');
$router->post('/ivtransaksipinjammeminjam', 'TransaksiController@iv');
$router->post('/ivpembayaranpinjaman', 'PembayaranController@iv');

// List Pendanaan Mitra - EMAS
$router->get('/listpendanaan', 'ListPendanaanController@index');

// Google Sheets
$router->post('/psheetdeposit', 'PSheetDepositController@index');
$router->post('/psheetpendanaan', 'PSheetPendanaanController@index');
$router->post('/psheetpelunasan', 'PSheetPelunasanController@index');
$router->post('/psheetvamitra', 'PSheetVaMitraController@index');
$router->post('/psheetwithdrawmitra', 'PSheetWithdrawMitraController@index');
$router->post('/psheetdepositmitra', 'PSheetDepositMitraController@index');
$router->post('/psheetchanneling', 'PSheetChannelingController@index');
$router->post('/psheetbank', 'PSheetBankController@index');

// $router->post('/gsheetsample', 'GSheetSampleController@index');
$router->post('/psheetsample', 'PSheetSampleController@index');

$router->post('/reg_borrower',[
    'uses' => 'RBorrowerController@index',
    'as' => 'RBorrowerController.index'
]);
$router->post('enc/reg_borrower',[
    'uses' => 'RBorrowerController@enc',
    'as' => 'RBorrowerController.enc'
]);
$router->post('iv/reg_borrower',[
    'uses' => 'RBorrowerController@iv',
    'as' => 'RBorrowerController.iv'
]);

$router->post('/reg_lender',[
    'uses' => 'RLenderController@index',
    'as' => 'RLenderController.index'
]);
$router->post('enc/reg_lender',[
    'uses' => 'RLenderController@enc',
    'as' => 'RLenderController.enc'
]);
$router->post('iv/reg_lender',[
    'uses' => 'RLenderController@iv',
    'as' => 'RLenderController.iv'
]);

$router->post('/reg_pengguna',[
    'uses' => 'RPenggunaController@index',
    'as' => 'RPenggunaController.index'
]);
$router->post('enc/reg_pengguna',[
    'uses' => 'RPenggunaController@enc',
    'as' => 'RPenggunaController.enc'
]);
$router->post('iv/reg_pengguna',[
    'uses' => 'RPenggunaController@iv',
    'as' => 'RPenggunaController.iv'
]);

$router->post('/pengajuan_pinjaman',[
    'uses' => 'PengPinjamanController@index',
    'as' => 'PengPinjamanController.index'
]);
$router->post('enc/pengajuan_pinjaman',[
    'uses' => 'PengPinjamanController@enc',
    'as' => 'PengPinjamanController.enc'
]);
$router->post('iv/pengajuan_pinjaman',[
    'uses' => 'PengPinjamanController@iv',
    'as' => 'PengPinjamanController.iv'
]);

$router->post('enc/pengajuan_pemberian_pinjaman',[
    'uses' => 'PemPinjamanController@enc',
    'as' => 'PemPinjamanController.enc'
]);
$router->post('iv/pengajuan_pemberian_pinjaman',[
    'uses' => 'PemPinjamanController@iv',
    'as' => 'PemPinjamanController.iv'
]);

$router->post('/pembayaran_pinjaman',[
    'uses' => 'PembayaranController@index',
    'as' => 'PembayaranController.index'
]);
$router->post('enc/pembayaran_pinjaman',[
    'uses' => 'PembayaranController@enc',
    'as' => 'PembayaranController.enc'
]);
$router->post('iv/pembayaran_pinjaman',[
    'uses' => 'PembayaranController@iv',
    'as' => 'PembayaranController.iv'
]);

$router->post('/transaksi_pinjam_meminjam',[
    'uses' => 'TransaksiController@index',
    'as' => 'TransaksiController.index'
]);
$router->post('enc/transaksi_pinjam_meminjam',[
    'uses' => 'TransaksiController@enc',
    'as' => 'TransaksiController.enc'
]);
$router->post('iv/transaksi_pinjam_meminjam',[
    'uses' => 'TransaksiController@iv',
    'as' => 'TransaksiController.iv'
]);

$router->post('/body',[
    'uses' => 'BodyController@index',
    'as' => 'BodyController.index'
]);
$router->post('enc/body',[
    'uses' => 'BodyController@enc',
    'as' => 'BodyController.enc'
]);
$router->post('iv/body',[
    'uses' => 'BodyController@iv',
    'as' => 'BodyController.iv'
]);
