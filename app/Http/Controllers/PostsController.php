<?php
namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * @OA\Get(
     *      path="/posts",
     *      summary="API Sample Posts",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(name="page",
     *          in="query",
     *          required=false,
     *          description="mulai dari 1",
     *          @OA\Schema(type="integer")
     *      ),
     * @OA\Parameter(name="search",
     *          in="query",
     *          required=false,
     *          description="email / nama",
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(
     *              @OA\Property(property="responsecode", type="integer", example="200"),
     *              @OA\Property(property="message", type="string", example="Success"),
     *              @OA\Property(property="data", type="object", 
     *                  @OA\Property(property="id", type="integer", example="14045"),
     *                  @OA\Property(property="name", type="string", example="alif")
     *              ),
     *          )
     *      ),
     * @OA\Response(
     *          response=400,
     *          description="Failed",
     *          @OA\JsonContent(
     *              @OA\Property(property="responsecode", type="integer", example="400"),
     *              @OA\Property(property="message", type="string", example="Failed"),
     *              @OA\Property(property="data", type="object", example="[]")
     *          )
     *      ),
     * )
     */
    public function index()
    {
        $posts = Post::OrderBy("id", "DESC")->paginate(10);
        return response()->json($posts, 200);
    }
}
