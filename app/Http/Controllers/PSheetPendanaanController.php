<?php
namespace App\Http\Controllers;

use App\Models\UserClient;
use App\Models\SellTrans;
use App\Models\Mitra;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\dates as dates;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use DB;

class PSheetPendanaanController extends Controller
{
    /**
     * @OA\Post(
     *      path="/psheetpendanaan",
     *      summary="Post To Google Sheet Dashboard (Tab Pendanaan)",
     *      description="ID = AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw URL = https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec",
     *      tags={"Google Sheet"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=false,
     *          description="tanggal transaksi tidak perlu diisi jika POST tanggal H-1, jika back date lebih dari 1 hari harus diisi, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(Request $request)
    {
        $client=new Client();
        if ($request->tanggal != '') {
            $tanggal = $request->tanggal;
        } else {
            $tanggal = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
        }

        // Captive
        // 1. Kospin 1 - marcella@serbamulia.co.id              ID : 114
        // 2. Kospin 2 - marcella.wirawan@kspserbamulia.co.id   ID : 21373
        // 3. KSU 1 - ksumuliaabadisj@gmail.com                 ID : 16887

        // Institusi
        // 1. KSU 2 - ksumuliaabadisj2@gmail.com                ID : 20132
        // 2. Ganesha - admin.danain@bankganesha.co.id          ID : 23656
        // 3. Kolosal - asal@kolosal.co.id                      ID : 23845
        // 4. BRI Agro - fakri.guswandi@work.briagro.co.id      ID : 26003

        // Gadai MAS
        // GADAI MAS JATIM PT                                   ID : 1
        // GADAI MAS DKI PT                                     ID : 2
        // GADAI MAS BALI PT                                    ID : 3
        // GADAI MAS NTB PT                                     ID : 118
        // GADAI MAS SULSEL PT                                  ID : 119
        // GADAI MAS KALTIM PT                                  ID : 120
        // MAS AGUNG SEJAHTERA PT                               ID : 384
        // Gadai Mulia Jabar PT                                 ID : 16816

        $institusis = array(
            array('Kategori' => 'Captive', 'idUserClient' => 114),
            array('Kategori' => 'Captive', 'idUserClient' => 21373),
            array('Kategori' => 'Captive', 'idUserClient' => 16887),
            array('Kategori' => 'Institusi','idUserClient' => 20132),
            array('Kategori' => 'Institusi', 'idUserClient' => 23656),
            array('Kategori' => 'Institusi', 'idUserClient' => 23845),
            array('Kategori' => 'Institusi', 'idUserClient' => 26003),
        ); 
        // return response()->json($institusis, 200);

        $items = array();
        
        // *** New Pendanaan ***
        foreach ($institusis as $institusi) {
            $NewPendanaan = DB::connection('mysql3')
                ->table('sell_trans')
                ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
                ->where(array('sell_trans.idUserClient' => $institusi['idUserClient'], 'sell_trans.tanggal' => $tanggal))
                ->groupBy('deb_pinjaman.idMitra')
                ->orderBy('deb_pinjaman.idMitra')
                ->selectRaw('SUM(sell_trans.amount) AS jumlah, tblmitra.idMitra, tblmitra.namaMitra')
                ->get();
            if ($NewPendanaan) {
                foreach ($NewPendanaan as $pendanaan) {
                    $UserClient = UserClient::find($institusi['idUserClient']);
                    $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=New Pendanaan&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&mitra='.$pendanaan->namaMitra.'&jumlah='.$pendanaan->jumlah.'&action=insertpendanaan');
                    $items[] = array(
                        'Tanggal'           => $tanggal, 
                        'Bulan'             => dates::bulanInd($tanggal), 
                        'Tahun'             => substr($tanggal, 0, 4), 
                        'Parameter'         => 'Pendanaan', 
                        'Jenis'             => 'New Pendanaan', 
                        'Kategori'          => $institusi['Kategori'], 
                        'Detail'            => $UserClient->userName, 
                        'Mitra'             => $pendanaan->namaMitra, 
                        'Jml Sistem Danain' => $pendanaan->jumlah,
                        'Upload GSheet'     => $google->getStatusCode(),
                    );
                    if (isset($jumlahNew[$pendanaan->idMitra])) $jumlahNew[$pendanaan->idMitra] += $pendanaan->jumlah;
                    else $jumlahNew[$pendanaan->idMitra] = $pendanaan->jumlah;
                }
            }        
        }
        $NewPendanaan = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
            // ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,1,2,3,118,119,120,384,16816,0])
            ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('sell_trans.tanggal', $tanggal)
            ->groupBy('deb_pinjaman.idMitra')
            ->orderBy('deb_pinjaman.idMitra')
            ->selectRaw('SUM(sell_trans.amount) AS jumlah, tblmitra.idMitra, tblmitra.namaMitra')
            ->get();
        if ($NewPendanaan) {
            foreach ($NewPendanaan as $pendanaan) {
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=New Pendanaan&kategori=Individu&detail=Individu&mitra='.$pendanaan->namaMitra.'&jumlah='.$pendanaan->jumlah.'&action=insertpendanaan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pendanaan', 
                    'Jenis'             => 'New Pendanaan', 
                    'Kategori'          => 'Individu', 
                    'Detail'            => 'Individu', 
                    'Mitra'             => $pendanaan->namaMitra, 
                    'Jml Sistem Danain' => $pendanaan->jumlah,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                if (isset($jumlahNew[$pendanaan->idMitra])) $jumlahNew[$pendanaan->idMitra] += $pendanaan->jumlah;
                else $jumlahNew[$pendanaan->idMitra] = $pendanaan->jumlah;
            }
        }
        $mitras = Mitra::all();
        $totalAllMitra = 0;
        foreach ($mitras as $mitra) {
            if (isset($jumlahNew[$mitra->idMitra])) {
                $totalMitra = $jumlahNew[$mitra->idMitra];
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=New Pendanaan&kategori=Total&detail=Total&mitra='.$mitra->namaMitra.'&jumlah='.$totalMitra.'&action=insertpendanaan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pendanaan', 
                    'Jenis'             => 'New Pendanaan', 
                    'Kategori'          => 'Total', 
                    'Detail'            => 'Total', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Jml Sistem Danain' => $totalMitra,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $totalAllMitra += $totalMitra;
            }
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=New Pendanaan&kategori=Total All Mitra&detail=Total All Mitra&mitra='.$mitra->namaMitra.'&jumlah='.$totalAllMitra.'&action=insertpendanaan');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Pendanaan', 
            'Jenis'             => 'New Pendanaan', 
            'Kategori'          => 'Total All Mitra', 
            'Detail'            => 'Total All Mitra', 
            'Jml Sistem Danain' => $totalAllMitra,
            'Upload GSheet'     => $google->getStatusCode(),
        );
        
        // *** Total Pendanaan ***
        foreach ($institusis as $institusi) {
            $TotalPendanaan = DB::connection('mysql3')
                ->table('sell_trans')
                ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
                ->where('sell_trans.idUserClient', $institusi['idUserClient'])
                ->where('sell_trans.tanggal', '<=', $tanggal)
                ->groupBy('deb_pinjaman.idMitra')
                ->orderBy('deb_pinjaman.idMitra')
                ->selectRaw('SUM(sell_trans.amount) AS jumlah, tblmitra.idMitra, tblmitra.namaMitra')
                ->get();
            if ($TotalPendanaan) {
                foreach ($TotalPendanaan as $pendanaan) {
                    $UserClient = UserClient::find($institusi['idUserClient']);
                    $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=Total Pendanaan&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&mitra='.$pendanaan->namaMitra.'&jumlah='.$pendanaan->jumlah.'&action=insertpendanaan');
                    $items[] = array(
                        'Tanggal'           => $tanggal, 
                        'Bulan'             => dates::bulanInd($tanggal), 
                        'Tahun'             => substr($tanggal, 0, 4), 
                        'Parameter'         => 'Pendanaan', 
                        'Jenis'             => 'Total Pendanaan', 
                        'Kategori'          => $institusi['Kategori'], 
                        'Detail'            => $UserClient->userName, 
                        'Mitra'             => $pendanaan->namaMitra, 
                        'Jml Sistem Danain' => $pendanaan->jumlah,
                        // 'Upload GSheet'     => $google->getStatusCode(),
                    );
                    if (isset($jumlahTotal[$pendanaan->idMitra])) $jumlahTotal[$pendanaan->idMitra] += $pendanaan->jumlah;
                    else $jumlahTotal[$pendanaan->idMitra] = $pendanaan->jumlah;
                }
            }
        }
        $TotalPendanaan = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
            // ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,1,2,3,118,119,120,384,16816,0])
            ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('sell_trans.tanggal', '<=', $tanggal)
            ->groupBy('deb_pinjaman.idMitra')
            ->orderBy('deb_pinjaman.idMitra')
            ->selectRaw('SUM(sell_trans.amount) AS jumlah, tblmitra.idMitra, tblmitra.namaMitra')
            ->get();
        if ($TotalPendanaan) {
            foreach ($TotalPendanaan as $pendanaan) {
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=Total Pendanaan&kategori=Individu&detail=Individu&mitra='.$pendanaan->namaMitra.'&jumlah='.$pendanaan->jumlah.'&action=insertpendanaan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pendanaan', 
                    'Jenis'             => 'Total Pendanaan', 
                    'Kategori'          => 'Individu', 
                    'Detail'            => 'Individu', 
                    'Mitra'             => $pendanaan->namaMitra, 
                    'Jml Sistem Danain' => $pendanaan->jumlah,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                if (isset($jumlahTotal[$pendanaan->idMitra])) $jumlahTotal[$pendanaan->idMitra] += $pendanaan->jumlah;
                else $jumlahTotal[$pendanaan->idMitra] = $pendanaan->jumlah;
            }
        }
        $mitras = Mitra::all();
        $totalAllMitra = 0;
        foreach ($mitras as $mitra) {
            if (isset($jumlahTotal[$mitra->idMitra])) {
                $totalMitra = $jumlahTotal[$mitra->idMitra];
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=Total Pendanaan&kategori=Total&detail=Total&mitra='.$mitra->namaMitra.'&jumlah='.$totalMitra.'&action=insertpendanaan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pendanaan', 
                    'Jenis'             => 'Total Pendanaan', 
                    'Kategori'          => 'Total', 
                    'Detail'            => 'Total', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Jml Sistem Danain' => $totalMitra,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $totalAllMitra += $totalMitra;
            }
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=Total Pendanaan&kategori=Total All Mitra&detail=Total All Mitra&mitra='.$mitra->namaMitra.'&jumlah='.$totalAllMitra.'&action=insertpendanaan');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Pendanaan', 
            'Jenis'             => 'Total Pendanaan', 
            'Kategori'          => 'Total All Mitra', 
            'Detail'            => 'Total All Mitra', 
            'Jml Sistem Danain' => $totalAllMitra,
            'Upload GSheet'     => $google->getStatusCode(),
        );

        // *** OS Pendanaan ***
        foreach ($institusis as $institusi) {
            $tanggal = $tanggal;
            $OSPendanaan = DB::connection('mysql3')
                ->table('sell_trans')
                ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
                ->where('sell_trans.idUserClient', $institusi['idUserClient'])
                ->where('sell_trans.tanggal', '<=', $tanggal)
                ->where(function($query) use ($tanggal) {
                    $query->whereNull('sell_trans.tgl_prepayment')
                    ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
                })
                ->groupBy('deb_pinjaman.idMitra')
                ->orderBy('deb_pinjaman.idMitra')
                ->selectRaw('SUM(sell_trans.amount) AS jumlah, tblmitra.idMitra, tblmitra.namaMitra')
                ->get();
            if ($OSPendanaan) {
                foreach ($OSPendanaan as $pendanaan) {
                    $UserClient = UserClient::find($institusi['idUserClient']);
                    $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=OS Pendanaan&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&mitra='.$pendanaan->namaMitra.'&jumlah='.$pendanaan->jumlah.'&action=insertpendanaan');
                    $items[] = array(
                        'Tanggal'           => $tanggal, 
                        'Bulan'             => dates::bulanInd($tanggal), 
                        'Tahun'             => substr($tanggal, 0, 4), 
                        'Parameter'         => 'Pendanaan', 
                        'Jenis'             => 'OS Pendanaan', 
                        'Kategori'          => $institusi['Kategori'], 
                        'Detail'            => $UserClient->userName, 
                        'Mitra'             => $pendanaan->namaMitra, 
                        'Jml Sistem Danain' => $pendanaan->jumlah,
                        'Upload GSheet'     => $google->getStatusCode(),
                    );
                    if (isset($jumlahOS[$pendanaan->idMitra])) $jumlahOS[$pendanaan->idMitra] += $pendanaan->jumlah;
                    else $jumlahOS[$pendanaan->idMitra] = $pendanaan->jumlah;
                }
            }
        }
        $tanggal = $tanggal;
        $OSPendanaan = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
            // ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,1,2,3,118,119,120,384,16816,0])
            ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('sell_trans.tanggal', '<=', $tanggal)
            ->where(function($query) use ($tanggal) {
                $query->whereNull('sell_trans.tgl_prepayment')
                ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
            })
            ->groupBy('deb_pinjaman.idMitra')
            ->orderBy('deb_pinjaman.idMitra')
            ->selectRaw('SUM(sell_trans.amount) AS jumlah, tblmitra.idMitra, tblmitra.namaMitra')
            ->get();
        if ($OSPendanaan) {
            foreach ($OSPendanaan as $pendanaan) {
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=OS Pendanaan&kategori=Individu&detail=Individu&mitra='.$pendanaan->namaMitra.'&jumlah='.$pendanaan->jumlah.'&action=insertpendanaan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pendanaan', 
                    'Jenis'             => 'OS Pendanaan', 
                    'Kategori'          => 'Individu', 
                    'Detail'            => 'Individu', 
                    'Mitra'             => $pendanaan->namaMitra, 
                    'Jml Sistem Danain' => $pendanaan->jumlah,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                if (isset($jumlahOS[$pendanaan->idMitra])) $jumlahOS[$pendanaan->idMitra] += $pendanaan->jumlah;
                else $jumlahOS[$pendanaan->idMitra] = $pendanaan->jumlah;
            }
        }
        $mitras = Mitra::all();
        $totalAllMitra = 0;
        foreach ($mitras as $mitra) {
            if (isset($jumlahOS[$mitra->idMitra])) {
                $totalMitra = $jumlahOS[$mitra->idMitra];
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=OS Pendanaan&kategori=Total&detail=Total&mitra='.$mitra->namaMitra.'&jumlah='.$totalMitra.'&action=insertpendanaan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pendanaan', 
                    'Jenis'             => 'OS Pendanaan', 
                    'Kategori'          => 'Total', 
                    'Detail'            => 'Total', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Jml Sistem Danain' => $totalMitra,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $totalAllMitra += $totalMitra;
            }
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pendanaan&jenis=OS Pendanaan&kategori=Total All Mitra&detail=Total All Mitra&mitra='.$mitra->namaMitra.'&jumlah='.$totalAllMitra.'&action=insertpendanaan');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Pendanaan', 
            'Jenis'             => 'OS Pendanaan', 
            'Kategori'          => 'Total All Mitra', 
            'Detail'            => 'Total All Mitra', 
            'Jml Sistem Danain' => $totalAllMitra,
            'Upload GSheet'     => $google->getStatusCode(),
        );

        // RCV : Penerimaan Investasi
        return response()->json($items, 200);

        // $posts = Post::select('id', 'title', 'status', 'content')->OrderBy("id", "ASC")->paginate(10);
        // return response()->json($posts, 200);
    }
}
