<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class UserCountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * @OA\Get(
     *      path="/api/usercount",
     *      summary="Get To Dashboard Metrics",
     *      security={{"bearerAuth":{}}},
     *      tags={"Dashboard Metrics"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=true,
     *          description="tanggal, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=409,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index(Request $request)
    {
        $tanggalLengkap = explode("-",$request->tanggal);
        $bulan = $tanggalLengkap[0]."-".$tanggalLengkap[1];
        $items = array();

        for ($tanggal=$bulan."-01"; $tanggal<=$request->tanggal; $tanggal++) {
            $NewUser = DB::connection('mysql3')
                ->table('tbluserclient')
                ->where('updated_at', 'like', '%'.$tanggal.'%')
                ->selectRaw('COUNT(idUserClient) AS jumlah')
                ->first();
            // $NewActiveUser = DB::connection('mysql3')
            //     ->table('sell_trans')
            //     ->where('tanggal', $tanggal)
            //     ->groupBy('idUserClient')
            //     ->havingRaw('jumlah = 1')
            //     ->selectRaw('COUNT(idUserClient) AS jumlah')
            //     ->get();
            $NewActiveUser = DB::connection('mysql3')
                ->table('sell_trans AS B')
                ->where('B.tanggal', $tanggal)
                ->whereIn('B.idUserClient', function($query){
                    $query->from('sell_trans AS A')
                    ->groupBy('A.idUserClient')
                    ->havingRaw('COUNT(A.idUserClient) = 1')
                    ->selectRaw('A.idUserClient')
                    ->get();
                })
                ->groupBy('B.idUserClient')
                ->selectRaw('B.idUserClient')
                ->get();
            $ActiveUser = DB::connection('mysql3')
                ->table('sell_trans')
                ->where(function($query) use ($tanggal) {
                    $query->whereNull('tgl_prepayment')
                    ->orWhere('tgl_prepayment', '>', $tanggal);
                })
                ->selectRaw('COUNT(distinct(idUserClient)) AS jumlah')
                ->first();
            // $Refferal = DB::connection('mysql3')
            //     ->table('sell_trans')
            //     ->join('referal', 'referal.idUserClient', '=', 'sell_trans.idUserClient')
            //     ->where('sell_trans.tanggal', $tanggal)
            //     ->groupBy('sell_trans.idUserClient')
            //     ->havingRaw('jumlah = 1')
            //     ->selectRaw('COUNT(sell_trans.idUserClient) AS jumlah')
            //     ->get();
            $Refferal = DB::connection('mysql3')
                ->table('sell_trans AS B')
                ->join('referal', 'referal.idUserClient', '=', 'B.idUserClient')
                ->where('B.tanggal', $tanggal)
                ->whereIn('B.idUserClient', function($query){
                    $query->from('sell_trans AS A')
                    ->groupBy('A.idUserClient')
                    ->havingRaw('COUNT(A.idUserClient) = 1')
                    ->selectRaw('A.idUserClient')
                    ->get();
                })
                ->groupBy('B.idUserClient')
                ->selectRaw('B.idUserClient')
                ->get();
            if (isset($totalAwal)) $Kenaikan = (($ActiveUser->jumlah - $totalAwal) / $totalAwal) * 100;
            else $Kenaikan = 0;
            $items[] = array(
                'Tanggal'           => $tanggal, 
                'NewUser'           => $NewUser->jumlah, 
                'NewActiveUser'     => COUNT($NewActiveUser), 
                'TotalActiveUser'   => $ActiveUser->jumlah, 
                'Kenaikan'          => round($Kenaikan,2)." %",
                'Referral'          => COUNT($Refferal)
            );
            $totalAwal = $ActiveUser->jumlah;
        }

        return response()->json($items, 200);
    }
}
