<?php
namespace App\Http\Controllers;

use App\Models\UserClient;
use App\Models\SellTrans;
use App\Models\Mitra;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Query\JoinClause;
use App\CustomClass\dates as dates;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use DB;

class PSheetPelunasanController extends Controller
{
    /**
     * @OA\Post(
     *      path="/psheetpelunasan",
     *      summary="Post To Google Sheet Dashboard (Tab Pelunasan)",
     *      description="ID = AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw URL = https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec",
     *      tags={"Google Sheet"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=false,
     *          description="tanggal transaksi tidak perlu diisi jika POST tanggal H-1, jika back date lebih dari 1 hari harus diisi, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(Request $request)
    {
        $client=new Client();
        if ($request->tanggal != '') {
            $tanggal = $request->tanggal;
        } else {
            $tanggal = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
        }

        // Captive
        // 1. Kospin 1 - marcella@serbamulia.co.id              ID : 114
        // 2. Kospin 2 - marcella.wirawan@kspserbamulia.co.id   ID : 21373
        // 3. KSU 1 - ksumuliaabadisj@gmail.com                 ID : 16887

        // Institusi
        // 1. KSU 2 - ksumuliaabadisj2@gmail.com                ID : 20132
        // 2. Ganesha - admin.danain@bankganesha.co.id          ID : 23656
        // 3. Kolosal - asal@kolosal.co.id                      ID : 23845
        // 4. BRI Agro - fakri.guswandi@work.briagro.co.id      ID : 26003

        // Gadai MAS
        // GADAI MAS JATIM PT                                   ID : 1
        // GADAI MAS DKI PT                                     ID : 2
        // GADAI MAS BALI PT                                    ID : 3
        // GADAI MAS NTB PT                                     ID : 118
        // GADAI MAS SULSEL PT                                  ID : 119
        // GADAI MAS KALTIM PT                                  ID : 120
        // MAS AGUNG SEJAHTERA PT                               ID : 384
        // Gadai Mulia Jabar PT                                 ID : 16816

        $institusis = array(
            array('Kategori' => 'Captive', 'idUserClient' => 114),
            array('Kategori' => 'Captive', 'idUserClient' => 21373),
            array('Kategori' => 'Captive', 'idUserClient' => 16887),
            array('Kategori' => 'Institusi','idUserClient' => 20132),
            array('Kategori' => 'Institusi', 'idUserClient' => 23656),
            array('Kategori' => 'Institusi', 'idUserClient' => 23845),
            array('Kategori' => 'Institusi', 'idUserClient' => 26003),
        ); 
        // return response()->json($institusis, 200);

        $items = array();
        
        // *** New Pelunasan ***
        foreach ($institusis as $institusi) {
            $NewPelunasan = DB::connection('mysql3')
                ->table('sell_trans')
                ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
                ->leftJoin('referal', 'referal.idUserClient', '=', 'sell_trans.idUserClient')
                ->leftJoin('kasbank', function ($join) {
                    $join->on('kasbank.idUserClient', '=', 'referal.idUserReferal')
                    ->on('kasbank.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                    ->where('kasbank.refNo', '=', 'Referal fee');
                })
		        ->where(array('sell_trans.idUserClient' => $institusi['idUserClient'], 'sell_trans.tgl_prepayment' => $tanggal))
                ->groupBy('deb_pinjaman.idMitra')
                ->orderBy('deb_pinjaman.idMitra')
                ->selectRaw('SUM(sell_trans.amount) AS jumlah, SUM(sell_trans.bunga) AS jumlahbunga, SUM(sell_trans.jasa) AS jumlahjasa, tblmitra.idMitra, tblmitra.namaMitra, SUM(kasbank.amount) AS referalfee')
                ->get();
            if ($NewPelunasan) {
                foreach ($NewPelunasan as $pelunasan) {
                    $UserClient = UserClient::find($institusi['idUserClient']);
                    $jumlahjasa = $pelunasan->jumlahjasa + $pelunasan->referalfee;
                    $Total = $pelunasan->jumlah + $pelunasan->jumlahbunga + $jumlahjasa;
                    $TotalBunga = $pelunasan->jumlahbunga + $jumlahjasa;
                    $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=New Pelunasan&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&mitra='.$pelunasan->namaMitra.'&pokok='.str_replace(".",",",$pelunasan->jumlah).'&bunga='.str_replace(".",",",$pelunasan->jumlahbunga).'&danain='.str_replace(".",",",$jumlahjasa).'&total='.str_replace(".",",",$Total).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
                    $items[] = array(
                        'Tanggal'           => $tanggal, 
                        'Bulan'             => dates::bulanInd($tanggal), 
                        'Tahun'             => substr($tanggal, 0, 4), 
                        'Parameter'         => 'Pelunasan', 
                        'Jenis'             => 'New Pelunasan', 
                        'Kategori'          => $institusi['Kategori'], 
                        'Detail'            => $UserClient->userName, 
                        'Mitra'             => $pelunasan->namaMitra, 
                        'Pokok'             => $pelunasan->jumlah,
                        'Bunga'             => $pelunasan->jumlahbunga,
                        'Danain'            => $jumlahjasa,
                        'Total'             => $Total, 
                        'Total Bunga'       => $TotalBunga, 
                        'Upload GSheet'     => $google->getStatusCode(),
                    );
                    if (isset($jumlahNew[$pelunasan->idMitra])) $jumlahNew[$pelunasan->idMitra] += $pelunasan->jumlah;
                    else $jumlahNew[$pelunasan->idMitra] = $pelunasan->jumlah;
                    if (isset($jumlahBungaNew[$pelunasan->idMitra])) $jumlahBungaNew[$pelunasan->idMitra] += $pelunasan->jumlahbunga;
                    else $jumlahBungaNew[$pelunasan->idMitra] = $pelunasan->jumlahbunga;
                    if (isset($jumlahJasaNew[$pelunasan->idMitra])) $jumlahJasaNew[$pelunasan->idMitra] += $jumlahjasa;
                    else $jumlahJasaNew[$pelunasan->idMitra] = $jumlahjasa;
                    if (isset($jumlahTotalNew[$pelunasan->idMitra])) $jumlahTotalNew[$pelunasan->idMitra] += $Total;
                    else $jumlahTotalNew[$pelunasan->idMitra] = $Total;
                    if (isset($jumlahTotalBungaNew[$pelunasan->idMitra])) $jumlahTotalBungaNew[$pelunasan->idMitra] += $TotalBunga;
                    else $jumlahTotalBungaNew[$pelunasan->idMitra] = $TotalBunga;
                }
            }        
        }
        $NewPelunasan = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
            ->leftJoin('referal', 'referal.idUserClient', '=', 'sell_trans.idUserClient')
            ->leftJoin('kasbank', function ($join) {
                $join->on('kasbank.idUserClient', '=', 'referal.idUserReferal')
                ->on('kasbank.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->where('kasbank.refNo', '=', 'Referal fee');
            })
		    ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('sell_trans.tgl_prepayment', $tanggal)
            ->groupBy('deb_pinjaman.idMitra')
            ->orderBy('deb_pinjaman.idMitra')
            ->selectRaw('SUM(sell_trans.amount) AS jumlah, SUM(sell_trans.bunga) AS jumlahbunga, SUM(sell_trans.jasa) AS jumlahjasa, tblmitra.idMitra, tblmitra.namaMitra, SUM(kasbank.amount) AS referalfee')
            ->get();
        if ($NewPelunasan) {
            foreach ($NewPelunasan as $pelunasan) {
                $jumlahjasa = $pelunasan->jumlahjasa + $pelunasan->referalfee;
                $Total = $pelunasan->jumlah + $pelunasan->jumlahbunga + $jumlahjasa;
                $TotalBunga = $pelunasan->jumlahbunga + $jumlahjasa;
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=New Pelunasan&kategori=Individu&detail=Individu&mitra='.$pelunasan->namaMitra.'&pokok='.str_replace(".",",",$pelunasan->jumlah).'&bunga='.str_replace(".",",",$pelunasan->jumlahbunga).'&danain='.str_replace(".",",",$jumlahjasa).'&total='.str_replace(".",",",$Total).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pelunasan', 
                    'Jenis'             => 'New Pelunasan', 
                    'Kategori'          => 'Individu', 
                    'Detail'            => 'Individu', 
                    'Mitra'             => $pelunasan->namaMitra, 
                    'Pokok'             => $pelunasan->jumlah,
                    'Bunga'             => $pelunasan->jumlahbunga,
                    'Danain'            => $jumlahjasa,
                    'Total'             => $Total, 
                    'Total Bunga'       => $TotalBunga, 
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                if (isset($jumlahNew[$pelunasan->idMitra])) $jumlahNew[$pelunasan->idMitra] += $pelunasan->jumlah;
                else $jumlahNew[$pelunasan->idMitra] = $pelunasan->jumlah;
                if (isset($jumlahBungaNew[$pelunasan->idMitra])) $jumlahBungaNew[$pelunasan->idMitra] += $pelunasan->jumlahbunga;
                else $jumlahBungaNew[$pelunasan->idMitra] = $pelunasan->jumlahbunga;
                if (isset($jumlahJasaNew[$pelunasan->idMitra])) $jumlahJasaNew[$pelunasan->idMitra] += $jumlahjasa;
                else $jumlahJasaNew[$pelunasan->idMitra] = $jumlahjasa;
                if (isset($jumlahTotalNew[$pelunasan->idMitra])) $jumlahTotalNew[$pelunasan->idMitra] += $Total;
                else $jumlahTotalNew[$pelunasan->idMitra] = $Total;
                if (isset($jumlahTotalBungaNew[$pelunasan->idMitra])) $jumlahTotalBungaNew[$pelunasan->idMitra] += $TotalBunga;
                else $jumlahTotalBungaNew[$pelunasan->idMitra] = $TotalBunga;
            }
        }
        $mitras = Mitra::all();
        $totalPokok = 0;
        $totalBunga = 0;
        $totalJasa = 0;
        $totalTotal = 0;
        $totalTotalBunga = 0;
        foreach ($mitras as $mitra) {
            if (isset($jumlahNew[$mitra->idMitra])) {
                $pokok = $jumlahNew[$mitra->idMitra];
                $bunga = $jumlahBungaNew[$mitra->idMitra];
                $jasa = $jumlahJasaNew[$mitra->idMitra];
                $Total = $jumlahTotalNew[$mitra->idMitra];
                $TotalBunga = $jumlahTotalBungaNew[$mitra->idMitra];
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=New Pelunasan&kategori=Total&detail=Total&mitra='.$mitra->namaMitra.'&pokok='.str_replace(".",",",$pokok).'&bunga='.str_replace(".",",",$bunga).'&danain='.str_replace(".",",",$jasa).'&total='.str_replace(".",",",$Total).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pelunasan', 
                    'Jenis'             => 'New Pelunasan', 
                    'Kategori'          => 'Total', 
                    'Detail'            => 'Total', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Pokok'             => $pokok,
                    'Bunga'             => $bunga,
                    'Danain'            => $jasa,
                    'Total'             => $Total, 
                    'Total Bunga'       => $TotalBunga, 
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $totalPokok += $pokok;
                $totalBunga += $bunga;
                $totalJasa += $jasa;
                $totalTotal += $Total;
                $totalTotalBunga += $TotalBunga;
            }
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=New Pelunasan&kategori=Total All Mitra&detail=Total All Mitra&mitra=Total All Mitra&pokok='.str_replace(".",",",$totalPokok).'&bunga='.str_replace(".",",",$totalBunga).'&danain='.str_replace(".",",",$totalJasa).'&total='.str_replace(".",",",$totalTotal).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Pelunasan', 
            'Jenis'             => 'New Pelunasan', 
            'Kategori'          => 'Total All Mitra', 
            'Detail'            => 'Total All Mitra', 
            'Mitra'             => 'Total All Mitra', 
            'Pokok'             => $totalPokok,
            'Bunga'             => $totalBunga,
            'Danain'            => $totalJasa,
            'Total'             => $totalTotal, 
            'Total Bunga'       => $totalTotalBunga, 
            'Upload GSheet'     => $google->getStatusCode(),
        );
        
        // *** Total Pelunasan ***
        foreach ($institusis as $institusi) {
            $TotalPelunasan = DB::connection('mysql3')
                ->table('sell_trans')
                ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
                ->leftJoin('referal', 'referal.idUserClient', '=', 'sell_trans.idUserClient')
                ->leftJoin('kasbank', function ($join) {
                    $join->on('kasbank.idUserClient', '=', 'referal.idUserReferal')
                    ->on('kasbank.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                    ->where('kasbank.refNo', '=', 'Referal fee');
                })
		        ->where('sell_trans.idUserClient', $institusi['idUserClient'])
                ->where('sell_trans.tgl_prepayment', '<=', $tanggal)
                ->groupBy('deb_pinjaman.idMitra')
                ->orderBy('deb_pinjaman.idMitra')
                ->selectRaw('SUM(sell_trans.amount) AS jumlah, SUM(sell_trans.bunga) AS jumlahbunga, SUM(sell_trans.jasa) AS jumlahjasa, tblmitra.idMitra, tblmitra.namaMitra, SUM(kasbank.amount) AS referalfee')
                ->get();
            if ($TotalPelunasan) {
                foreach ($TotalPelunasan as $pelunasan) {
                    $UserClient = UserClient::find($institusi['idUserClient']);
                    $jumlahjasa = $pelunasan->jumlahjasa + $pelunasan->referalfee;
                    $Total = $pelunasan->jumlah + $pelunasan->jumlahbunga + $jumlahjasa;
                    $TotalBunga = $pelunasan->jumlahbunga + $jumlahjasa;
                    $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=Total Pelunasan&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&mitra='.$pelunasan->namaMitra.'&pokok='.str_replace(".",",",$pelunasan->jumlah).'&bunga='.str_replace(".",",",$pelunasan->jumlahbunga).'&danain='.str_replace(".",",",$jumlahjasa).'&total='.str_replace(".",",",$Total).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
                    $items[] = array(
                        'Tanggal'           => $tanggal, 
                        'Bulan'             => dates::bulanInd($tanggal), 
                        'Tahun'             => substr($tanggal, 0, 4), 
                        'Parameter'         => 'Pelunasan', 
                        'Jenis'             => 'Total Pelunasan', 
                        'Kategori'          => $institusi['Kategori'], 
                        'Detail'            => $UserClient->userName, 
                        'Mitra'             => $pelunasan->namaMitra, 
                        'Pokok'             => $pelunasan->jumlah,
                        'Bunga'             => $pelunasan->jumlahbunga,
                        'Danain'            => $jumlahjasa,
                        'Total'             => $Total, 
                        'Total Bunga'       => $TotalBunga, 
                        'Upload GSheet'     => $google->getStatusCode(),
                    );
                    if (isset($jumlahTotal[$pelunasan->idMitra])) $jumlahTotal[$pelunasan->idMitra] += $pelunasan->jumlah;
                    else $jumlahTotal[$pelunasan->idMitra] = $pelunasan->jumlah;
                    if (isset($jumlahBungaTotal[$pelunasan->idMitra])) $jumlahBungaTotal[$pelunasan->idMitra] += $pelunasan->jumlahbunga;
                    else $jumlahBungaTotal[$pelunasan->idMitra] = $pelunasan->jumlahbunga;
                    if (isset($jumlahJasaTotal[$pelunasan->idMitra])) $jumlahJasaTotal[$pelunasan->idMitra] += $jumlahjasa;
                    else $jumlahJasaTotal[$pelunasan->idMitra] = $jumlahjasa;
                    if (isset($jumlahTotalTotal[$pelunasan->idMitra])) $jumlahTotalTotal[$pelunasan->idMitra] += $Total;
                    else $jumlahTotalTotal[$pelunasan->idMitra] = $Total;
                    if (isset($jumlahTotalBungaTotal[$pelunasan->idMitra])) $jumlahTotalBungaTotal[$pelunasan->idMitra] += $TotalBunga;
                    else $jumlahTotalBungaTotal[$pelunasan->idMitra] = $TotalBunga;
                }
            }
        }
        $TotalPelunasan = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
            ->leftJoin('referal', 'referal.idUserClient', '=', 'sell_trans.idUserClient')
            ->leftJoin('kasbank', function ($join) {
                $join->on('kasbank.idUserClient', '=', 'referal.idUserReferal')
                ->on('kasbank.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->where('kasbank.refNo', '=', 'Referal fee');
            })
		    ->whereNotIn('sell_trans.idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('sell_trans.tgl_prepayment', '<=', $tanggal)
            ->groupBy('deb_pinjaman.idMitra')
            ->orderBy('deb_pinjaman.idMitra')
            ->selectRaw('SUM(sell_trans.amount) AS jumlah, SUM(sell_trans.bunga) AS jumlahbunga, SUM(sell_trans.jasa) AS jumlahjasa, tblmitra.idMitra, tblmitra.namaMitra, SUM(kasbank.amount) AS referalfee')
            ->get();
        if ($TotalPelunasan) {
            foreach ($TotalPelunasan as $pelunasan) {
                $jumlahjasa = $pelunasan->jumlahjasa + $pelunasan->referalfee;
                $Total = $pelunasan->jumlah + $pelunasan->jumlahbunga + $jumlahjasa;
                $TotalBunga = $pelunasan->jumlahbunga + $jumlahjasa;
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=Total Pelunasan&kategori=Individu&detail=Individu&mitra='.$pelunasan->namaMitra.'&pokok='.str_replace(".",",",$pelunasan->jumlah).'&bunga='.str_replace(".",",",$pelunasan->jumlahbunga).'&danain='.str_replace(".",",",$jumlahjasa).'&total='.str_replace(".",",",$Total).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pelunasan', 
                    'Jenis'             => 'Total Pelunasan', 
                    'Kategori'          => 'Individu', 
                    'Detail'            => 'Individu', 
                    'Mitra'             => $pelunasan->namaMitra, 
                    'Pokok'             => $pelunasan->jumlah,
                    'Bunga'             => $pelunasan->jumlahbunga,
                    'Danain'            => $jumlahjasa,
                    'Total'             => $Total, 
                    'Total Bunga'       => $TotalBunga, 
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                if (isset($jumlahTotal[$pelunasan->idMitra])) $jumlahTotal[$pelunasan->idMitra] += $pelunasan->jumlah;
                else $jumlahTotal[$pelunasan->idMitra] = $pelunasan->jumlah;
                if (isset($jumlahBungaTotal[$pelunasan->idMitra])) $jumlahBungaTotal[$pelunasan->idMitra] += $pelunasan->jumlahbunga;
                else $jumlahBungaTotal[$pelunasan->idMitra] = $pelunasan->jumlahbunga;
                if (isset($jumlahJasaTotal[$pelunasan->idMitra])) $jumlahJasaTotal[$pelunasan->idMitra] += $jumlahjasa;
                else $jumlahJasaTotal[$pelunasan->idMitra] = $jumlahjasa;
                if (isset($jumlahTotalTotal[$pelunasan->idMitra])) $jumlahTotalTotal[$pelunasan->idMitra] += $Total;
                else $jumlahTotalTotal[$pelunasan->idMitra] = $Total;
                if (isset($jumlahTotalBungaTotal[$pelunasan->idMitra])) $jumlahTotalBungaTotal[$pelunasan->idMitra] += $TotalBunga;
                else $jumlahTotalBungaTotal[$pelunasan->idMitra] = $TotalBunga;
            }
        }
        $mitras = Mitra::all();
        $totalPokok = 0;
        $totalBunga = 0;
        $totalJasa = 0;
        $totalTotal = 0;
        $totalTotalBunga = 0;
        foreach ($mitras as $mitra) {
            if (isset($jumlahTotal[$mitra->idMitra])) {
                $pokok = $jumlahTotal[$mitra->idMitra];
                $bunga = $jumlahBungaTotal[$mitra->idMitra];
                $jasa = $jumlahJasaTotal[$mitra->idMitra];
                $Total = $jumlahTotalTotal[$mitra->idMitra];
                $TotalBunga = $jumlahTotalBungaTotal[$mitra->idMitra];
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=Total Pelunasan&kategori=Total&detail=Total&mitra='.$mitra->namaMitra.'&pokok='.str_replace(".",",",$pokok).'&bunga='.str_replace(".",",",$bunga).'&danain='.str_replace(".",",",$jasa).'&total='.str_replace(".",",",$Total).'&totalbunga='.str_replace(".",",",$TotalBunga).'&action=insertpelunasan');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Pelunasan', 
                    'Jenis'             => 'Total Pelunasan', 
                    'Kategori'          => 'Total', 
                    'Detail'            => 'Total', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Pokok'             => $pokok,
                    'Bunga'             => $bunga,
                    'Danain'            => $jasa,
                    'Total'             => $Total, 
                    'Total Bunga'       => $TotalBunga, 
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $totalPokok += $pokok;
                $totalBunga += $bunga;
                $totalJasa += $jasa;
                $totalTotal += $Total;
                $totalTotalBunga += $TotalBunga;
            }
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Pelunasan&jenis=Total Pelunasan&kategori=Total All Mitra&detail=Total All Mitra&mitra=Total All Mitra&pokok='.str_replace(".",",",$totalPokok).'&bunga='.str_replace(".",",",$totalBunga).'&danain='.str_replace(".",",",$totalJasa).'&total='.str_replace(".",",",$totalTotal).'&totalbunga='.str_replace(".",",",$totalTotalBunga).'&action=insertpelunasan');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Pelunasan', 
            'Jenis'             => 'Total Pelunasan', 
            'Kategori'          => 'Total All Mitra', 
            'Detail'            => 'Total All Mitra', 
            'Mitra'             => 'Total All Mitra', 
            'Pokok'             => $totalPokok,
            'Bunga'             => $totalBunga,
            'Danain'            => $totalJasa,
            'Total'             => $totalTotal, 
            'Total Bunga'       => $totalTotalBunga, 
            'Upload GSheet'     => $google->getStatusCode(),
        );

        // RCV : Penerimaan Investasi
        return response()->json($items, 200);
    }
}
