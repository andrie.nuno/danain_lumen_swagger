<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\DashboardUser;
use DB;

class ActiveUserPostController extends Controller
{
    /**
     * @OA\Post(
     *      path="/activeuser",
     *      summary="Get To Dashboard Metrics",
     *      tags={"Dashboard Metrics"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=false,
     *          description="periode tanggal, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=409,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index(Request $request)
    {
        if ($request->tanggal != '') {
            $tanggal = $request->tanggal;
        } else {
            $tanggal = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
        }
        // $items = array();

        $NewUser = DB::connection('mysql3')
            ->table('tbluserclient')
            ->where('tglJoin', '=', $tanggal)
            ->selectRaw('COUNT(idUserClient) AS jumlah')
            ->first();
        $NewUserActive = DB::connection('mysql3')
            ->table('sell_trans AS B')
            ->where('B.tanggal', $tanggal)
            ->whereIn('B.idUserClient', function($query){
                $query->from('sell_trans AS A')
                ->groupBy('A.idUserClient')
                ->havingRaw('COUNT(A.idUserClient) = 1')
                ->selectRaw('A.idUserClient')
                ->get();
            })
            ->groupBy('B.idUserClient')
            ->selectRaw('B.idUserClient')
            ->get();
        $TotalActiveUser = DB::connection('mysql3')
            ->table('sell_trans')
            ->where(function($query) use ($tanggal) {
                $query->whereNull('tgl_prepayment')
                ->orWhere('tgl_prepayment', '>', $tanggal);
            })
            ->selectRaw('COUNT(distinct(idUserClient)) AS jumlah')
            ->first();
        $Referal = DB::connection('mysql3')
            ->table('tbluserclient')
            ->join('referal', 'referal.idUserClient', '=', 'tbluserclient.idUserClient')
            ->where('tbluserclient.tglJoin', '=', $tanggal)
            ->selectRaw('COUNT(tbluserclient.idUserClient) AS jumlah')
            ->first();
        $OSRetail = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'deb_pinjaman.idUserClient')
            ->where('tbluserclient.isSystem', '=', 1)
            ->where('sell_trans.tanggal', '<=', $tanggal)
            ->where(function($query) use ($tanggal) {
                $query->whereNull('sell_trans.tgl_prepayment')
                ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
            })
            ->selectRaw('SUM(sell_trans.amount) AS jumlah')
            ->first();
        $SaldoRetail = DB::connection('mysql3')
            ->table('kasbank')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
            ->where('kasbank.tanggal', '<=', $tanggal)
            ->where('tbluserclient.isSystem', '=', 1)
            ->selectRaw('SUM(kasbank.amount) AS jumlah')
            ->first();
        $OSInstitusi = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'deb_pinjaman.idUserClient')
            ->where('tbluserclient.isSystem', '=', 2)
            ->where('sell_trans.tanggal', '<=', $tanggal)
            ->where(function($query) use ($tanggal) {
                $query->whereNull('sell_trans.tgl_prepayment')
                ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
            })
            ->selectRaw('SUM(sell_trans.amount) AS jumlah')
            ->first();
        $SaldoInstitusi = DB::connection('mysql3')
            ->table('kasbank')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
            ->where('kasbank.tanggal', '<=', $tanggal)
            ->where('tbluserclient.isSystem', '=', 2)
            ->selectRaw('SUM(kasbank.amount) AS jumlah')
            ->first();
        
        $query = DashboardUser::firstOrNew(array('tanggal' => $tanggal));
        $query->tanggal = $tanggal;
        $query->newUser = $NewUser->jumlah;
        $query->newUserActive = COUNT($NewUserActive);
        $query->totalActiveUser = $TotalActiveUser->jumlah;
        $query->referal = $Referal->jumlah;
        $query->OSRetail = $OSRetail->jumlah;
        $query->SaldoRetail = round($SaldoRetail->jumlah);
        $query->OSInstitusi = $OSInstitusi->jumlah;
        $query->SaldoInstitusi = round($SaldoInstitusi->jumlah);
        $query->save();

        $data = DashboardUser::find($tanggal);
        return response()->json($data, 200);
    }
}
