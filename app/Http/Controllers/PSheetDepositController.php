<?php
namespace App\Http\Controllers;

use App\Models\UserClient;
use App\Models\KasBank;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\dates as dates;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PSheetDepositController extends Controller
{
    /**
     * @OA\Post(
     *      path="/psheetdeposit",
     *      summary="Post To Google Sheet Dashboard (Tab Deposit)",
     *      description="ID = AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw URL = https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec",
     *      tags={"Google Sheet"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=false,
     *          description="tanggal transaksi tidak perlu diisi jika POST tanggal H-1, jika back date lebih dari 1 hari harus diisi, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(Request $request)
    {
        $client=new Client();

        // Captive
        // 1. Kospin 1 - marcella@serbamulia.co.id              ID : 114
        // 2. Kospin 2 - marcella.wirawan@kspserbamulia.co.id   ID : 21373
        // 3. KSU 1 - ksumuliaabadisj@gmail.com                 ID : 16887

        // Institusi
        // 1. KSU 2 - ksumuliaabadisj2@gmail.com                ID : 20132
        // 2. Ganesha - admin.danain@bankganesha.co.id          ID : 23656
        // 3. Kolosal - asal@kolosal.co.id                      ID : 23845
        // 4. BRI Agro - fakri.guswandi@work.briagro.co.id      ID : 26003

        // Gadai MAS
        // GADAI MAS JATIM PT                                   ID : 1
        // GADAI MAS DKI PT                                     ID : 2
        // GADAI MAS BALI PT                                    ID : 3
        // GADAI MAS NTB PT                                     ID : 118
        // GADAI MAS SULSEL PT                                  ID : 119
        // GADAI MAS KALTIM PT                                  ID : 120
        // MAS AGUNG SEJAHTERA PT                               ID : 384
        // Gadai Mulia Jabar PT                                 ID : 16816

        $institusis = array(
            array('Kategori' => 'Captive', 'idUserClient' => 114),
            array('Kategori' => 'Captive', 'idUserClient' => 21373),
            array('Kategori' => 'Captive', 'idUserClient' => 16887),
            array('Kategori' => 'Institusi','idUserClient' => 20132),
            array('Kategori' => 'Institusi', 'idUserClient' => 23656),
            array('Kategori' => 'Institusi', 'idUserClient' => 23845),
            array('Kategori' => 'Institusi', 'idUserClient' => 26003),
        ); 
        // return response()->json($institusis, 200);

        $items = array();
        if ($request->tanggal != '') {
            $tanggal = $request->tanggal;
        } else {
            $tanggal = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
        }
        
        // *** New Deposit ***
        $jmlNewDeposit = 0;
        foreach ($institusis as $institusi) {
            $NewDeposit = KasBank::selectRaw('SUM(amount) AS jumlah')
                ->whereIn('kdTrans', ['DEP','OTH1'])
                ->where(array('idUserClient' => $institusi['idUserClient'], 'tanggal' => $tanggal))
                ->groupBy('idUserClient')
                ->first();
            if ($NewDeposit) {
                $UserClient = UserClient::find($institusi['idUserClient']);
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Deposit&jenis=New Deposit&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&jumlah='.$NewDeposit->jumlah.'&action=insert');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Deposit', 
                    'Jenis'             => 'New Deposit', 
                    'Kategori'          => $institusi['Kategori'], 
                    'Detail'            => $UserClient->userName, 
                    'Jml Sistem Danain' => $NewDeposit->jumlah,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $jmlNewDeposit += $NewDeposit->jumlah;
            }            
        }
        $NewDeposit = KasBank::selectRaw('SUM(amount) AS jumlah')
            ->whereIn('kdTrans', ['DEP','OTH1'])
            // ->whereNotIn('idUserClient', [114,21373,16887,20132,23656,23845,26003,1,2,3,118,119,120,384,16816,0])
            ->whereNotIn('idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('tanggal', $tanggal)
            ->groupBy('tanggal')
            ->first();
        if ($NewDeposit) {
            $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Deposit&jenis=New Deposit&kategori=Individu&detail=Individu&jumlah='.$NewDeposit->jumlah.'&action=insert');
            $items[] = array(
                'Tanggal'           => $tanggal, 
                'Bulan'             => dates::bulanInd($tanggal), 
                'Tahun'             => substr($tanggal, 0, 4), 
                'Parameter'         => 'Deposit', 
                'Jenis'             => 'New Deposit', 
                'Kategori'          => 'Individu', 
                'Detail'            => 'Individu', 
                'Jml Sistem Danain' => $NewDeposit->jumlah,
                'Upload GSheet'     => $google->getStatusCode(),
            );
            $jmlNewDeposit += $NewDeposit->jumlah;
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Deposit&jenis=New Deposit&kategori=Total&detail=Total&jumlah='.$jmlNewDeposit.'&action=insert');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Deposit', 
            'Jenis'             => 'New Deposit', 
            'Kategori'          => 'Total', 
            'Detail'            => 'Total', 
            'Jml Sistem Danain' => $jmlNewDeposit,
            'Upload GSheet'     => $google->getStatusCode(),
        );
        
        // *** Total Deposit ***
        $jmlTotalDeposit = 0;
        foreach ($institusis as $institusi) {
            $TotalDeposit = KasBank::selectRaw('SUM(amount) AS jumlah')
                ->whereIn('kdTrans', ['DEP','OTH1'])
                ->where('idUserClient', $institusi['idUserClient'])
                ->where('tanggal', '<=', $tanggal)
                ->groupBy('idUserClient')
                ->first();
            if ($TotalDeposit) {
                $UserClient = UserClient::find($institusi['idUserClient']);
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Deposit&jenis=Total Deposit&kategori='.$institusi['Kategori'].'&detail='.$UserClient->userName.'&jumlah='.$TotalDeposit->jumlah.'&action=insert');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Parameter'         => 'Deposit', 
                    'Jenis'             => 'Total Deposit', 
                    'Kategori'          => $institusi['Kategori'], 
                    'Detail'            => $UserClient->userName, 
                    'Jml Sistem Danain' => $TotalDeposit->jumlah,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $jmlTotalDeposit += $TotalDeposit->jumlah;
            }            
        }
        $TotalDeposit = KasBank::selectRaw('SUM(amount) AS jumlah')
            ->whereIn('kdTrans', ['DEP','OTH1'])
            // ->whereNotIn('idUserClient', [114,21373,16887,20132,23656,23845,26003,1,2,3,118,119,120,384,16816,0])
            ->whereNotIn('idUserClient', [114,21373,16887,20132,23656,23845,26003,0])
            ->where('tanggal', '<=', $tanggal)
            ->first();
        if ($TotalDeposit) {
            $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Deposit&jenis=Total Deposit&kategori=Individu&detail=Individu&jumlah='.$TotalDeposit->jumlah.'&action=insert');
            $items[] = array(
                'Tanggal'           => $tanggal, 
                'Bulan'             => dates::bulanInd($tanggal), 
                'Tahun'             => substr($tanggal, 0, 4), 
                'Parameter'         => 'Deposit', 
                'Jenis'             => 'Total Deposit', 
                'Kategori'          => 'Individu', 
                'Detail'            => 'Individu', 
                'Jml Sistem Danain' => $TotalDeposit->jumlah,
                'Upload GSheet'     => $google->getStatusCode(),
            );
            $jmlTotalDeposit += $TotalDeposit->jumlah;
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&param=Deposit&jenis=Total Deposit&kategori=Total&detail=Total&jumlah='.$jmlTotalDeposit.'&action=insert');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Parameter'         => 'Deposit', 
            'Jenis'             => 'Total Deposit', 
            'Kategori'          => 'Total', 
            'Detail'            => 'Total', 
            'Jml Sistem Danain' => $jmlTotalDeposit,
            'Upload GSheet'     => $google->getStatusCode(),
        );
        
        return response()->json($items, 200);
    }
}
