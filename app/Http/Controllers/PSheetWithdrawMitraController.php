<?php
namespace App\Http\Controllers;

use App\Models\UserClient;
use App\Models\Mitra;
use App\Models\MitraVaTrans;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\dates as dates;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use DB;

class PSheetWithdrawMitraController extends Controller
{
    /**
     * @OA\Post(
     *      path="/psheetwithdrawmitra",
     *      summary="Post To Google Sheet Dashboard (Tab WithdrawMitra)",
     *      description="ID = AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw URL = https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec",
     *      tags={"Google Sheet"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=false,
     *          description="tanggal transaksi tidak perlu diisi jika POST tanggal H-1, jika back date lebih dari 1 hari harus diisi, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(Request $request)
    {
        $client=new Client();
        if ($request->tanggal != '') {
            $tanggal = $request->tanggal;
        } else {
            $tanggal = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));
        }

        // Captive
        // 1. Kospin 1 - marcella@serbamulia.co.id              ID : 114
        // 2. Kospin 2 - marcella.wirawan@kspserbamulia.co.id   ID : 21373
        // 3. KSU 1 - ksumuliaabadisj@gmail.com                 ID : 16887

        // Institusi
        // 1. KSU 2 - ksumuliaabadisj2@gmail.com                ID : 20132
        // 2. Ganesha - admin.danain@bankganesha.co.id          ID : 23656
        // 3. Kolosal - asal@kolosal.co.id                      ID : 23845
        // 4. BRI Agro - fakri.guswandi@work.briagro.co.id      ID : 26003

        // Gadai MAS
        // GADAI MAS JATIM PT                                   ID : 1
        // GADAI MAS DKI PT                                     ID : 2
        // GADAI MAS BALI PT                                    ID : 3
        // GADAI MAS NTB PT                                     ID : 118
        // GADAI MAS SULSEL PT                                  ID : 119
        // GADAI MAS KALTIM PT                                  ID : 120
        // MAS AGUNG SEJAHTERA PT                               ID : 384
        // Gadai Mulia Jabar PT                                 ID : 16816

        $institusis = array(
            array('Kategori' => 'Captive', 'idUserClient' => 114),
            array('Kategori' => 'Captive', 'idUserClient' => 21373),
            array('Kategori' => 'Captive', 'idUserClient' => 16887),
            array('Kategori' => 'Institusi','idUserClient' => 20132),
            array('Kategori' => 'Institusi', 'idUserClient' => 23656),
            array('Kategori' => 'Institusi', 'idUserClient' => 23845),
            array('Kategori' => 'Institusi', 'idUserClient' => 26003),
        ); 
        // return response()->json($institusis, 200);

        $items = array();
        
        // *** Withdraw Mitra New ***
        $mitras = Mitra::all();
        $totalAllMitra = 0;
        foreach ($mitras as $mitra) {
            $VA = MitraVaTrans::selectRaw('SUM(nilaiPinjaman * -1) AS jumlah')
                ->where(array('idMitra' => $mitra->idMitra, 'tanggal' => $tanggal))
                ->whereIn('status', ['Tarik Dana','Waiting'])
                ->groupBy('idMitra')
                ->first();
            if ($VA) {
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&jenis=Penarikan&kategori=Penarikan&mitra='.$mitra->namaMitra.'&jumlah='.str_replace(".",",",$VA->jumlah).'&action=insertwdmitra');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Jenis'             => 'Penarikan', 
                    'Kategori'          => 'Penarikan', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Jml Sistem Danain' => $VA->jumlah,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
                $totalAllMitra += $VA->jumlah;
            } else {
                $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&jenis=Penarikan&kategori=Penarikan&mitra='.$mitra->namaMitra.'&jumlah=0&action=insertwdmitra');
                $items[] = array(
                    'Tanggal'           => $tanggal, 
                    'Bulan'             => dates::bulanInd($tanggal), 
                    'Tahun'             => substr($tanggal, 0, 4), 
                    'Jenis'             => 'Penarikan', 
                    'Kategori'          => 'Penarikan', 
                    'Mitra'             => $mitra->namaMitra, 
                    'Jml Sistem Danain' => 0,
                    'Upload GSheet'     => $google->getStatusCode(),
                );
            }
        }
        $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&jenis=Penarikan&kategori=Penarikan&mitra=Total&jumlah='.str_replace(".",",",$totalAllMitra).'&action=insertwdmitra');
        $items[] = array(
            'Tanggal'           => $tanggal, 
            'Bulan'             => dates::bulanInd($tanggal), 
            'Tahun'             => substr($tanggal, 0, 4), 
            'Jenis'             => 'Penarikan', 
            'Kategori'          => 'Penarikan', 
            'Mitra'             => 'Total', 
            'Jml Sistem Danain' => $totalAllMitra,
            'Upload GSheet'     => $google->getStatusCode(),
        );

        // *** Withdraw Mitra Total ***
        // $mitras = Mitra::all();
        // $totalAllMitra = 0;
        // foreach ($mitras as $mitra) {
        //     $VA = MitraVaTrans::selectRaw('SUM(nilaiPinjaman) AS jumlah')
        //         ->where(array('idMitra' => $mitra->idMitra, 'status' => 'Tarik Dana'))
        //         ->where('tanggal', '<=', $tanggal)
        //         ->groupBy('idMitra')
        //         ->first();
        //     if ($VA) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&jenis=Withdraw Mitra&kategori=Withdraw Mitra Total&mitra='.$mitra->namaMitra.'&jumlah='.str_replace(".",",",$VA->jumlah).'&action=insertwdmitra');
        //         $items[] = array(
        //             'Tanggal'           => $tanggal, 
        //             'Bulan'             => dates::bulanInd($tanggal), 
        //             'Tahun'             => substr($tanggal, 0, 4), 
        //             'Jenis'             => 'Withdraw Mitra', 
        //             'Kategori'          => 'Withdraw Mitra Total', 
        //             'Mitra'             => $mitra->namaMitra, 
        //             'Jml Sistem Danain' => $VA->jumlah,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //         $totalAllMitra += $VA->jumlah;
        //     } else {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&jenis=Withdraw Mitra&kategori=Withdraw Mitra Total&mitra='.$mitra->namaMitra.'&jumlah=0&action=insertvamitra');
        //         $items[] = array(
        //             'Tanggal'           => $tanggal, 
        //             'Bulan'             => dates::bulanInd($tanggal), 
        //             'Tahun'             => substr($tanggal, 0, 4), 
        //             'Jenis'             => 'Withdraw Mitra', 
        //             'Kategori'          => 'Withdraw Mitra Total', 
        //             'Mitra'             => $mitra->namaMitra, 
        //             'Jml Sistem Danain' => 0,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        // }
        // $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$tanggal.'&bulan='.dates::bulanInd($tanggal).'&tahun='.substr($tanggal, 0, 4).'&jenis=Withdraw Mitra&kategori=Withdraw Mitra Total&mitra=Total&jumlah='.str_replace(".",",",$totalAllMitra).'&action=insertwdmitra');
        // $items[] = array(
        //     'Tanggal'           => $tanggal, 
        //     'Bulan'             => dates::bulanInd($tanggal), 
        //     'Tahun'             => substr($tanggal, 0, 4), 
        //     'Jenis'             => 'Withdraw Mitra', 
        //     'Kategori'          => 'Withdraw Mitra Total', 
        //     'Mitra'             => 'Total', 
        //     'Jml Sistem Danain' => $totalAllMitra,
        //     'Upload GSheet'     => $google->getStatusCode(),
        // );

        // RCV : Penerimaan Investasi
        return response()->json($items, 200);

        // $posts = Post::select('id', 'title', 'status', 'content')->OrderBy("id", "ASC")->paginate(10);
        // return response()->json($posts, 200);
    }
}
