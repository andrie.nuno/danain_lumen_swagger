<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class NewUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * @OA\Get(
     *      path="/api/newuser",
     *      summary="Get To Dashboard Metrics",
     *      security={{"bearerAuth":{}}},
     *      tags={"Dashboard Metrics"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=true,
     *          description="tanggal, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=409,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index(Request $request)
    {
        $tanggalLengkap = explode("-",$request->tanggal);
        $tanggal = $request->tanggal;
        $bulan = $tanggalLengkap[0]."-".$tanggalLengkap[1];
        $items = array();

        $NewUser = DB::connection('mysql3')
            ->table('tbluserclient')
            ->where('updated_at', 'like', '%'.$bulan.'%')
            ->selectRaw('COUNT(idUserClient) AS jumlah')
            ->first();
        
        $ActiveUser = DB::connection('mysql3')
            ->table('sell_trans')
            ->where(function($query) use ($tanggal) {
                $query->whereNull('tgl_prepayment')
                ->orWhere('tgl_prepayment', '>', $tanggal);
            })
            ->selectRaw('COUNT(distinct(idUserClient)) AS jumlah')
            ->first();
        
        $items[] = array(
            'Tanggal'           => $request->tanggal, 
            'NewUser'           => $NewUser->jumlah, 
            'TotalActiveUser'   => $ActiveUser->jumlah, 
        );

        return response()->json($items, 200);
    }
}
