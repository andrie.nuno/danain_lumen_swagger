<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\DashboardUser;
use DB;

class ActiveUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * @OA\Get(
     *      path="/api/activeuser",
     *      summary="Get To Dashboard Metrics",
     *      security={{"bearerAuth":{}}},
     *      tags={"Dashboard Metrics"},
     *      @OA\Parameter(
     *          name="tanggal_awal",
     *          in="query",
     *          required=true,
     *          description="periode tanggal awal, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *          name="tanggal_akhir",
     *          in="query",
     *          required=true,
     *          description="periode tanggal akhir, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=409,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index(Request $request)
    {
        $tanggalAwal = $request->tanggal_awal;
        $tanggalAkhir = $request->tanggal_akhir;
        $items = array();

        $ActiveUser = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'sell_trans.idUserClient')
            ->where('sell_trans.tanggal', '>=', $tanggalAwal)
            ->where('sell_trans.tanggal', '<=', $tanggalAkhir)
            ->where('tbluserclient.isSystem', '=', 1)
            ->selectRaw('COUNT(distinct(sell_trans.idUserClient)) AS jumlah')
            ->first();
        $NewActiveUser = DB::connection('mysql3')
            ->table('sell_trans AS B')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'B.idUserClient')
            ->where('B.tanggal', '>=', $tanggalAwal)
            ->where('B.tanggal', '<=', $tanggalAkhir)
            ->where('tbluserclient.isSystem', '=', 1)
            ->whereNotIn('B.idUserClient', function($query) use ($tanggalAwal){
                $query->from('sell_trans AS A')
                ->where('A.tanggal', '<', $tanggalAwal)
                ->groupBy('A.idUserClient')
                ->selectRaw('A.idUserClient')
                ->get();
            })
            ->groupBy('B.idUserClient')
            ->selectRaw('B.idUserClient')
            ->get();
        $ActiveUserAwal = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'sell_trans.idUserClient')
            ->where('tbluserclient.isSystem', '=', 1)
            ->where(function($query) use ($tanggalAwal) {
                $query->whereNull('sell_trans.tgl_prepayment')
                ->orWhere('sell_trans.tgl_prepayment', '>', $tanggalAwal);
            })
            ->selectRaw('COUNT(distinct(sell_trans.idUserClient)) AS jumlah')
            ->first();
        $RetentionRate = round((($ActiveUser->jumlah - COUNT($NewActiveUser)) / $ActiveUserAwal->jumlah) * 100, 2);
        $ChurnRate = round(100 - $RetentionRate, 2);

        $Pendanaan = DB::connection('mysql3')
            ->table('sell_trans')
            ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'sell_trans.idUserClient')
            ->where('sell_trans.tanggal', '>=', $tanggalAwal)
            ->where('sell_trans.tanggal', '<=', $tanggalAkhir)
            ->where('tbluserclient.isSystem', '=', 1)
            ->groupBy('sell_trans.idUserClient')
            ->orderBy('jumlah')
            ->selectRaw('distinct(COUNT(sell_trans.idUserClient)) AS jumlah')
            ->get();
        if ($Pendanaan) {
            foreach ($Pendanaan as $data) {
                $JumlahPendana = DB::connection('mysql3')
                    ->table('sell_trans')
                    ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'sell_trans.idUserClient')
                    ->where('sell_trans.tanggal', '>=', $tanggalAwal)
                    ->where('sell_trans.tanggal', '<=', $tanggalAkhir)
                    ->where('tbluserclient.isSystem', '=', 1)
                    ->groupBy('sell_trans.idUserClient')
                    ->havingRaw('jumlah = '.$data->jumlah)
                    ->selectRaw('COUNT(sell_trans.idUserClient) AS jumlah')
                    ->get();
                $items[] = array(
                    'JumlahPendanaan'   => $data->jumlah.'xPendanaan', 
                    'JumlahUser'        => COUNT($JumlahPendana)
                    
                );
            }
        }

        // $TigaPendanaan = DB::connection('mysql3')
        //     ->table('sell_trans')
        //     ->where('tanggal', '>=', $tanggalAwal)
        //     ->where('tanggal', '<=', $tanggalAkhir)
        //     ->groupBy('idUserClient')
        //     ->havingRaw('jumlah = 3')
        //     ->selectRaw('COUNT(idUserClient) AS jumlah')
        //     ->get();
        // $DuaPendanaan = DB::connection('mysql3')
        //     ->table('sell_trans')
        //     ->where('tanggal', '>=', $tanggalAwal)
        //     ->where('tanggal', '<=', $tanggalAkhir)
        //     ->groupBy('idUserClient')
        //     ->havingRaw('jumlah = 2')
        //     ->selectRaw('COUNT(idUserClient) AS jumlah')
        //     ->get();
        // $SatuPendanaan = DB::connection('mysql3')
        //     ->table('sell_trans')
        //     ->where('tanggal', '>=', $tanggalAwal)
        //     ->where('tanggal', '<=', $tanggalAkhir)
        //     ->groupBy('idUserClient')
        //     ->havingRaw('jumlah = 1')
        //     ->selectRaw('COUNT(idUserClient) AS jumlah')
        //     ->get();
        
        // $items[] = array(
        //     'Period'                => $tanggalAwal." - ".$tanggalAkhir, 
        //     'UniqActiveUser'        => $ActiveUser->jumlah, 
        //     // '3xPendanaan'           => COUNT($TigaPendanaan), 
        //     // '2xPendanaan'           => COUNT($DuaPendanaan), 
        //     // '1xPendanaan'           => COUNT($SatuPendanaan), 
        //     // 'NewActiveUser'         => COUNT($NewActiveUser), 
        //     // 'UniqActiveUserAwal'    => $ActiveUserAwal->jumlah,
        //     // 'RetentionRate'         => $RetentionRate, 
        //     // 'ChurnRate'             => $ChurnRate
        // );

        // return response()->json($items, 200);

        return response()->json([
            'response' => [
                'status'                => 200, 
                'message'               => "OK", 
                'Period'                => $tanggalAwal." - ".$tanggalAkhir, 
                'UniqActiveUser'        => $ActiveUser->jumlah, 
                'NewActiveUser'         => COUNT($NewActiveUser), 
                'UniqActiveUserAwal'    => $ActiveUserAwal->jumlah, 
                'RetentionRate'         => $RetentionRate, 
                'ChurnRate'             => $ChurnRate, 
                'data'                  => $items
            ]
        ],200);

    }
}
