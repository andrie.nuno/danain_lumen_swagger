<?php
namespace App\Http\Controllers;

use App\Models\UserClient;
use App\Models\SellTrans;
use App\Models\Mitra;
use App\Models\KasBank;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\dates as dates;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use DB;

class PSheetBankController extends Controller
{
    /**
     * @OA\Post(
     *      path="/psheetbank",
     *      summary="Post To Google Sheet Dashboard Danain FA (Tab DanainBank)",
     *      description="ID = AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw URL = https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec",
     *      tags={"Google Sheet"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=true,
     *          description="tanggal transaksi, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(Request $request)
    {
        $client=new Client();

        $items = array();

        // foreach ($institusis as $institusi) {
        $Banks = DB::connection('mysql3')
            ->table('tblbank_ho')
            ->selectRaw('*')
            ->get();
        if ($Banks) {
            $totalBank = 0;
            foreach ($Banks as $Bank) {
                $Saldo = DB::connection('mysql3')
                    ->table('trans_other')
                    ->where('idBank', $Bank->idBank)
                    ->where('tglRekap', '<=', $request->tanggal)
                    ->groupBy('tglRekap')
                    ->selectRaw('SUM(amount) AS jumlah')
                    ->first();
                if ($Saldo) {
                    $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&jenis=Bank Statement&kategori=Off Balance Sheet&bank='.$Bank->anRekening.' - '.$Bank->noRekening.'&jumlah='.$Saldo->jumlah.'&action=insertbank');
                    $items[] = array(
                        'Tanggal'           => $request->tanggal, 
                        'Bulan'             => dates::bulanInd($request->tanggal), 
                        'Tahun'             => substr($request->tanggal, 0, 4), 
                        'Jenis'             => 'Bank Statement', 
                        'Kategori'          => 'Off Balance Sheet', 
                        'Bank'              => $Bank->anRekening.' - '.$Bank->noRekening, 
                        'Jml Sistem Danain' => $Saldo->jumlah,
                        'Upload GSheet'     => $google->getStatusCode(),
                    );
                    $totalBank += $Saldo->jumlah;
                }
            }
            $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&jenis=Bank Statement&kategori=Off Balance Sheet&bank=Total&jumlah='.$totalBank.'&action=insertbank');
            $items[] = array(
                'Tanggal'           => $request->tanggal, 
                'Bulan'             => dates::bulanInd($request->tanggal), 
                'Tahun'             => substr($request->tanggal, 0, 4), 
                'Jenis'             => 'Bank Statement', 
                'Kategori'          => 'Off Balance Sheet', 
                'Bank'              => 'Total', 
                'Jml Sistem Danain' => $totalBank,
                'Upload GSheet'     => $google->getStatusCode(),
            );
        }

        //     $UserClient = UserClient::find($institusi['idUserClient']);
        //     $mitras = Mitra::all();
        //     // *** Pendanaan ***
        //     $totalPendanaan = 0;
        //     foreach ($mitras as $mitra) {
        //         $Pendanaan = DB::connection('mysql3')
        //             ->table('sell_trans')
        //             ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //             ->where(array('sell_trans.idUserClient' => $institusi['idUserClient'], 'sell_trans.tanggal' => $request->tanggal, 'deb_pinjaman.idMitra' => $mitra->idMitra))
        //             ->groupBy('deb_pinjaman.idMitra')
        //             ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //             ->first();
        //         if ($Pendanaan) {
        //             $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pendanaan&kategori=Pokok&mitra='.$mitra->namaMitra.'&jumlah='.$Pendanaan->jumlah.'&action=insertchanneling');
        //             $items[] = array(
        //                 'Tanggal'           => $request->tanggal, 
        //                 'Bulan'             => dates::bulanInd($request->tanggal), 
        //                 'Tahun'             => substr($request->tanggal, 0, 4), 
        //                 'Bank'              => $UserClient->userName, 
        //                 'Jenis'             => 'Pendanaan', 
        //                 'Kategori'          => 'Pokok', 
        //                 'Mitra'             => $mitra->namaMitra, 
        //                 'Jml Sistem Danain' => $Pendanaan->jumlah,
        //                 'Upload GSheet'     => $google->getStatusCode(),
        //             );
        //             $totalPendanaan += $Pendanaan->jumlah;
        //         }
        //     }
        //     if ($totalPendanaan != 0) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pendanaan&kategori=Pokok&mitra=Total&jumlah='.$totalPendanaan.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Pendanaan', 
        //             'Kategori'          => 'Pokok', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $totalPendanaan,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        //     // *** Pelunasan Pokok ***
        //     $totalPelunasanPokok = 0;
        //     foreach ($mitras as $mitra) {
        //         $PelunasanPokok = DB::connection('mysql3')
        //             ->table('sell_trans')
        //             ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //             ->where(array('sell_trans.idUserClient' => $institusi['idUserClient'], 'sell_trans.tgl_prepayment' => $request->tanggal, 'deb_pinjaman.idMitra' => $mitra->idMitra))
        //             ->groupBy('deb_pinjaman.idMitra')
        //             ->orderBy('deb_pinjaman.idMitra')
        //             ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //             ->first();
        //         if ($PelunasanPokok) {
        //             $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pelunasan&kategori=Pokok&mitra='.$mitra->namaMitra.'&jumlah='.$PelunasanPokok->jumlah.'&action=insertchanneling');
        //             $items[] = array(
        //                 'Tanggal'           => $request->tanggal, 
        //                 'Bulan'             => dates::bulanInd($request->tanggal), 
        //                 'Tahun'             => substr($request->tanggal, 0, 4), 
        //                 'Bank'              => $UserClient->userName, 
        //                 'Jenis'             => 'Pelunasan', 
        //                 'Kategori'          => 'Pokok', 
        //                 'Mitra'             => $mitra->namaMitra, 
        //                 'Jml Sistem Danain' => $PelunasanPokok->jumlah,
        //                 'Upload GSheet'     => $google->getStatusCode(),
        //             );
        //             $totalPelunasanPokok += $PelunasanPokok->jumlah;
        //         }
        //     }
        //     if ($totalPelunasanPokok != 0) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pelunasan&kategori=Pokok&mitra=Total&jumlah='.$totalPelunasanPokok.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Pelunasan', 
        //             'Kategori'          => 'Pokok', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $totalPelunasanPokok,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        //     // *** Pelunasan Bunga ***
        //     $totalPelunasanBunga = 0;
        //     foreach ($mitras as $mitra) {
        //         $PelunasanBunga = DB::connection('mysql3')
        //             ->table('sell_trans')
        //             ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //             ->where(array('sell_trans.idUserClient' => $institusi['idUserClient'], 'sell_trans.tgl_prepayment' => $request->tanggal, 'deb_pinjaman.idMitra' => $mitra->idMitra))
        //             ->groupBy('deb_pinjaman.idMitra')
        //             ->orderBy('deb_pinjaman.idMitra')
        //             ->selectRaw('SUM(sell_trans.bunga) AS jumlah')
        //             ->first();
        //         if ($PelunasanBunga) {
        //             $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pelunasan&kategori=Bunga&mitra='.$mitra->namaMitra.'&jumlah='.$PelunasanBunga->jumlah.'&action=insertchanneling');
        //             $items[] = array(
        //                 'Tanggal'           => $request->tanggal, 
        //                 'Bulan'             => dates::bulanInd($request->tanggal), 
        //                 'Tahun'             => substr($request->tanggal, 0, 4), 
        //                 'Bank'              => $UserClient->userName, 
        //                 'Jenis'             => 'Pelunasan', 
        //                 'Kategori'          => 'Bunga', 
        //                 'Mitra'             => $mitra->namaMitra, 
        //                 'Jml Sistem Danain' => $PelunasanBunga->jumlah,
        //                 'Upload GSheet'     => $google->getStatusCode(),
        //             );
        //             $totalPelunasanBunga += $PelunasanBunga->jumlah;
        //         }
        //     }
        //     if ($totalPelunasanBunga != 0) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pelunasan&kategori=Bunga&mitra=Total&jumlah='.$totalPelunasanBunga.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Pelunasan', 
        //             'Kategori'          => 'Bunga', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $totalPelunasanBunga,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        //     // *** Deposit ***
        //     $Deposit = KasBank::selectRaw('SUM(amount) AS jumlah')
        //         ->whereIn('kdTrans', ['DEP','OTH1'])
        //         ->where(array('idUserClient' => $institusi['idUserClient'], 'tanggal' => $request->tanggal))
        //         ->groupBy('idUserClient')
        //         ->first();
        //     if ($Deposit) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Deposit&kategori=Deposit&mitra=Total&jumlah='.$Deposit->jumlah.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Deposit', 
        //             'Kategori'          => 'Deposit', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $Deposit->jumlah,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        //     // *** Pembayaran ***
        //     if ($institusi['idUserClient'] == 23656) {
        //         $Pembayaran = DB::connection('mysql3')
        //             ->table('trans_other')
        //             ->where('keterangan', 'like', '%'.'Pelunasan Bank Ganesha'.'%')
        //             ->where('tanggal', $request->tanggal)
        //             ->groupBy('tanggal')
        //             ->selectRaw('SUM(amount * -1) AS jumlah')
        //             ->first();
        //         if ($Pembayaran) {
        //             $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pembayaran&kategori=Pembayaran&mitra=Total&jumlah='.$Pembayaran->jumlah.'&action=insertchanneling');
        //             $items[] = array(
        //                 'Tanggal'           => $request->tanggal, 
        //                 'Bulan'             => dates::bulanInd($request->tanggal), 
        //                 'Tahun'             => substr($request->tanggal, 0, 4), 
        //                 'Bank'              => $UserClient->userName, 
        //                 'Jenis'             => 'Pembayaran', 
        //                 'Kategori'          => 'Pembayaran', 
        //                 'Mitra'             => 'Total', 
        //                 'Jml Sistem Danain' => $Pembayaran->jumlah,
        //                 'Upload GSheet'     => $google->getStatusCode(),
        //             );
        //         }
        //     }
        //     if ($institusi['idUserClient'] == 26003) {
        //         $Pembayaran = DB::connection('mysql3')
        //             ->table('trans_other')
        //             ->where('keterangan', 'like', '%'.'Pelunasan Bank BRI Argo'.'%')
        //             ->where('tanggal', $request->tanggal)
        //             ->groupBy('tanggal')
        //             ->selectRaw('SUM(amount * -1) AS jumlah')
        //             ->first();
        //         if ($Pembayaran) {
        //             $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Pembayaran&kategori=Pembayaran&mitra=Total&jumlah='.$Pembayaran->jumlah.'&action=insertchanneling');
        //             $items[] = array(
        //                 'Tanggal'           => $request->tanggal, 
        //                 'Bulan'             => dates::bulanInd($request->tanggal), 
        //                 'Tahun'             => substr($request->tanggal, 0, 4), 
        //                 'Bank'              => $UserClient->userName, 
        //                 'Jenis'             => 'Pembayaran', 
        //                 'Kategori'          => 'Pembayaran', 
        //                 'Mitra'             => 'Total', 
        //                 'Jml Sistem Danain' => $Pembayaran->jumlah,
        //                 'Upload GSheet'     => $google->getStatusCode(),
        //             );
        //         }
        //     }
        //     // *** OS Pendanaan ***
        //     $totalOS = 0;
        //     foreach ($mitras as $mitra) {
        //         $tanggal = $request->tanggal;
        //         $OSPendanaan = DB::connection('mysql3')
        //             ->table('sell_trans')
        //             ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //             ->where('sell_trans.idUserClient', $institusi['idUserClient'])
        //             ->where('sell_trans.tanggal', '<=', $request->tanggal)
        //             ->where('deb_pinjaman.idMitra', $mitra->idMitra)
        //             ->where(function($query) use ($tanggal) {
        //                 $query->whereNull('sell_trans.tgl_prepayment')
        //                 ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
        //             })
        //             ->groupBy('deb_pinjaman.idMitra')
        //             ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //             ->first();
        //         if ($OSPendanaan) {
        //             $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=OS Pendanaan&kategori=OS Pendanaan&mitra='.$mitra->namaMitra.'&jumlah='.$OSPendanaan->jumlah.'&action=insertchanneling');
        //             $items[] = array(
        //                 'Tanggal'           => $request->tanggal, 
        //                 'Bulan'             => dates::bulanInd($request->tanggal), 
        //                 'Tahun'             => substr($request->tanggal, 0, 4), 
        //                 'Bank'              => $UserClient->userName, 
        //                 'Jenis'             => 'OS Pendanaan', 
        //                 'Kategori'          => 'OS Pendanaan', 
        //                 'Mitra'             => $mitra->namaMitra, 
        //                 'Jml Sistem Danain' => $OSPendanaan->jumlah,
        //                 'Upload GSheet'     => $google->getStatusCode(),
        //             );
        //             $totalOS += $OSPendanaan->jumlah;
        //         }
        //     }
        //     if ($totalOS != 0) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=OS Pendanaan&kategori=OS Pendanaan&mitra=Total&jumlah='.$totalOS.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'OS Pendanaan', 
        //             'Kategori'          => 'OS Pendanaan', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $totalOS,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        //     // *** Aging ***
        //     $totalAging = 0;
        //     $tanggal = $request->tanggal;
        //     $tanggal30 = date('Y-m-d',strtotime('-30 days', strtotime($request->tanggal)));
        //     $AgingA = DB::connection('mysql3')
        //         ->table('sell_trans')
        //         ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //         ->where('sell_trans.idUserClient', $institusi['idUserClient'])
        //         ->where('sell_trans.tanggal', '>=', $tanggal30)
        //         ->where('sell_trans.tanggal', '<=', $request->tanggal)
        //         ->where(function($query) use ($tanggal) {
        //             $query->whereNull('sell_trans.tgl_prepayment')
        //             ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
        //         })
        //         ->groupBy('sell_trans.idUserClient')
        //         ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //         ->first();
        //     if ($AgingA) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Aging&kategori=A. 1-30 Days&mitra=Total&jumlah='.$AgingA->jumlah.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Aging', 
        //             'Kategori'          => 'A. 1-30 Days', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $AgingA->jumlah,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //         $totalAging += $AgingA->jumlah;
        //     }
        //     $tanggal60 = date('Y-m-d',strtotime('-60 days', strtotime($request->tanggal)));
        //     $AgingB = DB::connection('mysql3')
        //         ->table('sell_trans')
        //         ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //         ->where('sell_trans.idUserClient', $institusi['idUserClient'])
        //         ->where('sell_trans.tanggal', '>=', $tanggal60)
        //         ->where('sell_trans.tanggal', '<', $tanggal30)
        //         ->where(function($query) use ($tanggal) {
        //             $query->whereNull('sell_trans.tgl_prepayment')
        //             ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
        //         })
        //         ->groupBy('sell_trans.idUserClient')
        //         ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //         ->first();
        //     if ($AgingB) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Aging&kategori=B. 31-60 Days&mitra=Total&jumlah='.$AgingB->jumlah.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Aging', 
        //             'Kategori'          => 'B. 31-60 Days', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $AgingB->jumlah,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //         $totalAging += $AgingB->jumlah;
        //     }
        //     $tanggal90 = date('Y-m-d',strtotime('-90 days', strtotime($request->tanggal)));
        //     $AgingC = DB::connection('mysql3')
        //         ->table('sell_trans')
        //         ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //         ->where('sell_trans.idUserClient', $institusi['idUserClient'])
        //         ->where('sell_trans.tanggal', '>=', $tanggal90)
        //         ->where('sell_trans.tanggal', '<', $tanggal60)
        //         ->where(function($query) use ($tanggal) {
        //             $query->whereNull('sell_trans.tgl_prepayment')
        //             ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
        //         })
        //         ->groupBy('sell_trans.idUserClient')
        //         ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //         ->first();
        //     if ($AgingC) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Aging&kategori=C. 61-90 Days&mitra=Total&jumlah='.$AgingC->jumlah.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Aging', 
        //             'Kategori'          => 'C. 61-90 Days', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $AgingC->jumlah,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //         $totalAging += $AgingC->jumlah;
        //     }
        //     $tanggal120 = date('Y-m-d',strtotime('-120 days', strtotime($request->tanggal)));
        //     $AgingD = DB::connection('mysql3')
        //         ->table('sell_trans')
        //         ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
        //         ->where('sell_trans.idUserClient', $institusi['idUserClient'])
        //         ->where('sell_trans.tanggal', '>=', $tanggal120)
        //         ->where('sell_trans.tanggal', '<', $tanggal90)
        //         ->where(function($query) use ($tanggal) {
        //             $query->whereNull('sell_trans.tgl_prepayment')
        //             ->orWhere('sell_trans.tgl_prepayment', '>', $tanggal);
        //         })
        //         ->groupBy('sell_trans.idUserClient')
        //         ->selectRaw('SUM(sell_trans.amount) AS jumlah')
        //         ->first();
        //     if ($AgingD) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Aging&kategori=D. 91-120 Days&mitra=Total&jumlah='.$AgingD->jumlah.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Aging', 
        //             'Kategori'          => 'D. 91-120 Days', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $AgingD->jumlah,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //         $totalAging += $AgingD->jumlah;
        //     }
        //     if ($totalAging != 0) {
        //         $google = $client->get('https://script.google.com/macros/s/AKfycbzQ9hXMFJVgdYu6n3xoY5Kpisc4OxwqUj1ve1kKkksgouutThrFRolDOw/exec?callback=ctrlq&tanggal='.$request->tanggal.'&bulan='.dates::bulanInd($request->tanggal).'&tahun='.substr($request->tanggal, 0, 4).'&bank='.$UserClient->userName.'&jenis=Aging&kategori=E. < 120 Days&mitra=Total&jumlah='.$totalAging.'&action=insertchanneling');
        //         $items[] = array(
        //             'Tanggal'           => $request->tanggal, 
        //             'Bulan'             => dates::bulanInd($request->tanggal), 
        //             'Tahun'             => substr($request->tanggal, 0, 4), 
        //             'Bank'              => $UserClient->userName, 
        //             'Jenis'             => 'Aging', 
        //             'Kategori'          => 'E. < 120 Days', 
        //             'Mitra'             => 'Total', 
        //             'Jml Sistem Danain' => $totalAging,
        //             'Upload GSheet'     => $google->getStatusCode(),
        //         );
        //     }
        // }

        return response()->json($items, 200);

        // $posts = Post::select('id', 'title', 'status', 'content')->OrderBy("id", "ASC")->paginate(10);
        // return response()->json($posts, 200);
    }
}
