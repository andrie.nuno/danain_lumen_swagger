<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\helpers as help;
use App\Models\ListPendanaan;

class ListPendanaanController extends Controller
{
    /**
     * @OA\Get(
     *      path="/listpendanaan",
     *      summary="List Pendanaan By Tanggal",
     *      description="List Pendanaan Emas dari Mitra Gadai MAS dan Gadai Mulia",
     *      tags={"List Pendanaan Mitra - EMAS"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=true,
     *          description="Tanggal List Pendanaan, format YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          in="query",
     *          required=false,
     *          description="Numeric of Page, 1 Page is 20 data's", 
     *          @OA\Schema(type="number")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public static function index(Request $request)
    {
        $datas = ListPendanaan::where('id','!=',0)->paginate(20)->toArray();
        return response()->json($datas, 200);
    }
}

