<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\helpers as help;
use App\Models\Pembayaran;
use App\Models\PemPinjaman;
use App\Models\PengPinjaman;
use App\Models\RegBorrower;
use App\Models\RegLender;
use App\Models\RegPengguna;
use App\Models\Transaksi;

class BodyController extends Controller
{
    public function index(Request $request)
    {
        $array = [
            'reg_pengguna'=>RPenggunaController::index($request),
            'reg_borrower'=>RBorrowerController::index($request),
            'reg_lender'=>RLenderController::index($request),
            'pengajuan_pinjaman'=>PengPinjamanController::index($request),
            'pengajuan_pemberian_pinjaman'=>PemPinjamanController::index($request),
            'transaksi_pinjam_meminjam'=>TransaksiController::index($request),
            'pembayaran_pinjaman'=>PembayaranController::index($request)
        ];
        return $array;
    }

    public function enc(Request $request)
    {
        $array = [
            'reg_pengguna'=>RPenggunaController::enc($request),
            'reg_borrower'=>RBorrowerController::enc($request),
            'reg_lender'=>RLenderController::enc($request),
            'pengajuan_pinjaman'=>PengPinjamanController::enc($request),
            'pengajuan_pemberian_pinjaman'=>PemPinjamanController::enc($request),
            'transaksi_pinjam_meminjam'=>TransaksiController::enc($request),
            'pembayaran_pinjaman'=>PembayaranController::enc($request)
        ];
        return $array;
    }

    /**
     * @OA\Post(
     *      path="/ivbodypusdafil",
     *      summary="Pusdafil Body",
     *      tags={"Data Pusdafil"},
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function iv(Request $request)
    {
        $array = [
            'reg_pengguna'=>RPenggunaController::iv($request),
            'reg_borrower'=>RBorrowerController::iv($request),
            'reg_lender'=>RLenderController::iv($request),
            'pengajuan_pinjaman'=>PengPinjamanController::iv($request),
            'pengajuan_pemberian_pinjaman'=>PemPinjamanController::iv($request),
            'transaksi_pinjam_meminjam'=>TransaksiController::iv($request),
            'pembayaran_pinjaman'=>PembayaranController::iv($request)
        ];
        return $array;
    }
}

