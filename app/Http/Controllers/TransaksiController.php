<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomClass\helpers as help;
use App\Models\Transaksi;

class TransaksiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/transaksipinjammeminjam",
     *      summary="transaksi_pinjam_meminjam After Encrypt",
     *      tags={"Pusdafil Before Encrypt"},
     *      @OA\Parameter(
     *          name="page",
     *          in="query",
     *          required=false,
     *          description="Numeric of Page, 1 Page is 10 data's", 
     *          @OA\Schema(type="number")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public static function index()
    {
        $datas = Transaksi::paginate(10)->toArray();
        return response()->json($datas, 200);
    }

    /**
     * @OA\Post(
     *      path="/enctransaksipinjammeminjam",
     *      summary="transaksi_pinjam_meminjam Encrypt",
     *      description="encryption = base64_encode(openssl_encrypt(json, method='aes-256-cbc', password='4bd393e7a457f9023d9ba95fffb5a2e1', OPENSSL_RAW_DATA, iv='ijzh84t1w9xa56s9'))",
     *      tags={"Pusdafil Encrypt"},
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          required=true,
     *          description="Limit Get Data's", 
     *          @OA\Schema(type="number")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public static function enc(Request $request)
    {
        $limit = isset($request->limit)&&$request->limit!=''?$request->limit:null;
        $data = Transaksi::getdata($limit);
        $enc = help::encrypt($data,'enc');
        return $enc;
    }

    /**
     * @OA\Post(
     *      path="/ivtransaksipinjammeminjam",
     *      summary="transaksi_pinjam_meminjam Encrypt Concat IV",
     *      description="encryption = base64_encode(encryption::iv='ijzh84t1w9xa56s9')",
     *      tags={"Pusdafil Encrypt Concat IV"},
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          required=true,
     *          description="Limit Get Data's", 
     *          @OA\Schema(type="number")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public static function iv(Request $request)
    {
        $limit = isset($request->limit)&&$request->limit!=''?$request->limit:null;
        $data = Transaksi::getdata($limit);
        $enc = help::encrypt($data,'iv');
        return $enc;
    }
}

