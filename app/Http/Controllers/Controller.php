<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\SecurityScheme(
 *          type="http",
 *          in="header",
 *          scheme="bearer",
 *          bearerFormat="JWT",
 *          securityScheme="bearerAuth",
 * ), 
 * Class Controller
 * @package App\Http\Controllers
 * @OA\OpenApi(
 *      @OA\Info(
 *          version="1.0.0",
 *          title="Danain API Service App Doc",
 *          @OA\License(name="DANAIN")
 *      ),
 *      @OA\Server(
 *          description="API Danain Server",
 *          url="https://danain_lumen_swagger.test/",
 *      ),
 *      @OA\Server(
 *          description="API Onboarding Server",
 *          url="https://api-masborrower.danain.co.id/",
 *      ),
 *      @OA\Server(
 *          description="API Onboarding Server",
 *          url="https://script.google.com/macros/s/AKfycbzsKdS1CfIuFfGJHk8Ve3QqZ2GEOtkFCdqFP-FW_f3-BhPRD7Doqa9O/exec",
 *      ),
 *      @OA\Tag(
 *          name="Autentifikasi",
 *          description="Autentifikasi User"
 *      ),
 *      @OA\Tag(
 *          name="Data Pusdafil",
 *          description="API Body Data Pusdafil"
 *      ),
 *      @OA\Tag(
 *          name="Pusdafil Before Encrypt",
 *          description="API Endpoints of Pusdafil Before Encrypt"
 *      ),
 *      @OA\Tag(
 *          name="Pusdafil Encrypt",
 *          description="API Endpoints of Pusdafil Encrypt"
 *      ),
 *      @OA\Tag(
 *          name="Pusdafil Encrypt Concat IV",
 *          description="API Endpoints of Pusdafil Encrypt Concat IV"
 *      ),
 *      @OA\Tag(
 *          name="List Pendanaan Mitra - EMAS",
 *          description="API Dengan Mitra Gadai MAS dan Gadai Mulia untuk Pendanaan Emas"
 *      ),
 *      @OA\Tag(
 *          name="Onboarding",
 *          description="API Endpoints of Onboarding Borrower"
 *      ),
 *      @OA\Tag(
 *          name="Google Sheet",
 *          description="API Endpoints of Google Sheet"
 *      ),
 *      @OA\Tag(
 *          name="Dashboard Metrics",
 *          description="API Endpoints of Dashboard Metrics"
 *      ),
 * )
 */
class Controller extends BaseController
{
    public function respondWithToken($token)
    {
        return response()->json([
            'token'         => $token,
            'token_type'    => 'bearer',
            'expires_in'    => null
        ], 200);
    }
}
