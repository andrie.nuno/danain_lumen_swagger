<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class PenyerapanInstitusiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * @OA\Get(
     *      path="/api/penyerapaniinstitusi",
     *      summary="Get To Dashboard Metrics",
     *      security={{"bearerAuth":{}}},
     *      tags={"Dashboard Metrics"},
     *      @OA\Parameter(
     *          name="tanggal",
     *          in="query",
     *          required=true,
     *          description="tanggal, format : YYYY-MM-DD", 
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=409,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index(Request $request)
    {
        $tanggalLengkap = explode("-",$request->tanggal);
        $date = $request->tanggal;
        $bulan = $tanggalLengkap[0]."-".$tanggalLengkap[1];
        $items = array();

        for ($tanggal=$bulan."-01"; $tanggal<=$request->tanggal; $tanggal++) {
            $SaldoKemarin = DB::connection('mysql3')
                ->table('kasbank')
                ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
                ->where('kasbank.tanggal', '<', $tanggal)
                ->where('tbluserclient.isSystem', '=', 2)
                ->selectRaw('SUM(kasbank.amount) AS jumlah')
                ->first();
            $DepositHariIni = DB::connection('mysql3')
                ->table('kasbank')
                ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
                ->where('kasbank.tanggal', '=', $tanggal)
                ->where('kasbank.amount', '>', 0)
                ->where('tbluserclient.isSystem', '=', 2)
                ->selectRaw('SUM(kasbank.amount) AS jumlah')
                ->first();
            $SaldoTersedia = round($SaldoKemarin->jumlah + $DepositHariIni->jumlah);
            $NewPendanaan = DB::connection('mysql3')
                ->table('sell_trans')
                ->join('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'sell_trans.idDebPinjaman')
                ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'deb_pinjaman.idUserClient')
                ->where('sell_trans.tanggal', '=', $tanggal)
                ->where('tbluserclient.isSystem', '=', 2)
                ->selectRaw('SUM(sell_trans.amount) AS jumlah')
                ->first();
            $Penyerapan = round(($NewPendanaan->jumlah / ($NewPendanaan->jumlah + $SaldoTersedia)) * 100);

            $items[] = array(
                'Tanggal'           => $tanggal, 
                'SaldoTersedia'     => $SaldoTersedia, 
                'OSRetail'          => $NewPendanaan->jumlah, 
                'Penyerapan'        => $Penyerapan
            );
        }

        return response()->json($items, 200);
    }
}
