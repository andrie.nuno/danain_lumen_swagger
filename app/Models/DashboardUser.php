<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardUser extends Model
{
    protected $table = "dashboard_user";
    protected $primaryKey = 'tanggal';
    protected $fillable = [
        'tanggal',
        'newUser', 
        'newUserActive',
        'totalActiveUser',
        'referal',
        'OSRetail',
        'SaldoRetail',
        'OSInstitusi',
        'SaldoInstitusi'
    ];

    protected $casts=[
        'tanggal'=>'string',
    ];
}
