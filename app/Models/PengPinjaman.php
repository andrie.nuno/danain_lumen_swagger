<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengPinjaman extends Model
{

    protected $table = "pengajuan_pinjaman";
	protected $primaryKey = 'id_pinjaman';
    protected $fillable = [
        'id_penyelenggara',
        'id_pinjaman',
        'id_borrower',
        'id_syariah',
        'id_status_pengajuan_pinjaman',
        'nama_pinjaman',
        'tgl_pengajuan_pinjaman',
        'nilai_permohonan_pinjaman',
        'jangka_waktu_pinjaman',
        'satuan_jangka_waktu_pinjaman',
        'penggunaan_pinjaman',
        'agunan',
        'jenis_agunan',
        'rasio_pinjaman_nilai_agunan',
        'permintaan_jaminan',
        'rasio_pinjaman_aset',
        'cicilan_bulan',
        'rating_pengajuan_pinjaman',
        'nilai_plafond',
        'nilai_pengajuan_pinjaman',
        'suku_bunga_pinjaman',
        'satuan_suku_bunga_pinjaman',
        'jenis_bunga',
        'tgl_mulai_publikasi_pinjaman',
        'rencana_jangka_waktu_publikasi',
        'realisasi_jangka_waktu_publikasi',
        'tgl_mulai_pendanaan',
        'frekuensi_pinjaman'
    ];
    public $incrementing = true;
    
    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pinjaman'=>'string',
        'id_borrower'=>'string',
        'nama_pinjaman'=>'string',
        'penggunaan_pinjaman'=>'string',
        'permintaan_jaminan'=>'string',
        'rating_pengajuan_pinjaman'=>'string',
    ];
    
    static function getdata($limit=null){
        $data = PengPinjaman::select("*")->limit($limit)->get();
        return $data;
    }
}