<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegLender extends Model
{
    protected $table = "reg_lender";
    protected $primaryKey = 'id_pengguna';
    protected $fillable = [
        'id_penyelenggara',
        'id_pengguna', 
        'id_lender',
        'id_negara_domisili',
        'id_kewarganegaraan',
        'sumber_dana'
    ];

    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pengguna'=>'string',
        'id_lender'=>'string',
        'sumber_dana'=>'string'
    ];

    static function getdata($limit=null){
        $data = RegLender::select("*")->limit($limit)->get();
        return $data;
    }
}
