<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellTrans extends Model
{
    protected $table = "sell_trans";
    protected $connection = 'mysql3';
	protected $primaryKey = 'idInvestorSell';
    protected $fillable = ['idDebPinjaman', 'idUserClient','refNo','tanggal','amount','keterangan','isActive','tglTerima','ketTerima','tgl_proses','userId','bunga','jasa','tgl_pelunasan','tgl_prepayment','durasipengembalian'];
}
