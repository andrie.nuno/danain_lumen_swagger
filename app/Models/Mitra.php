<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    protected $table = "tblmitra";
    protected $connection = 'mysql3';
	protected $primaryKey = 'idMitra';
    protected $fillable = ['idMitra', 'namaMitra'];
}
