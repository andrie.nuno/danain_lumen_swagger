<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegPengguna extends Model
{
    protected $table = "reg_pengguna";
    protected $primaryKey = 'id_pengguna';
    protected $fillable = [
        'id_penyelenggara',
        'id_pengguna', 
        'jenis_pengguna',
        'tgl_registrasi',
        'nama_pengguna',
        'jenis_identitas',
        'no_identitas',
        'no_npwp',
        'id_jenis_badan_hukum',
        'tempat_lahir',
        'tgl_lahir',
        'id_jenis_kelamin',
        'alamat',
        'id_kota',
        'id_provinsi',
        'kode_pos',
        'id_agama',
        'id_status_perkawinan',
        'id_pekerjaan',
        'id_bidang_pekerjaan',
        'id_pekerjaan_online',
        'pendapatan',
        'pengalaman_kerja',
        'id_pendidikan',
        'nama_perwakilan',
        'no_identitas_perwakilan'
    ];

    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pengguna'=>'string',
        'nama_pengguna'=>'string',
        'no_identitas'=>'string',
        'no_npwp'=>'string',
        'tempat_lahir'=>'string',
        'alamat'=>'string',
        'id_kota'=>'string',
        'kode_pos'=>'string',
        'id_bidang_pekerjaan'=>'string',
    ];

    static function getdata($limit=null){
        $data = RegPengguna::select("*")->limit($limit)->get();
        return $data;
    }
}
