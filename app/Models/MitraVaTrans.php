<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MitraVaTrans extends Model
{
    protected $table = "mitra_va_trans";
    protected $connection = 'mysql3';
	protected $primaryKey = 'id';
    protected $fillable = ['idMitra', 'tanggal', 'nilaiPinjaman', 'bungaPendana','bungaDanain'];
}
