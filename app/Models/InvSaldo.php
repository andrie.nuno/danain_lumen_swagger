<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvSaldo extends Model
{
    protected $table = "inv_saldo";
    protected $connection = 'mysql3';
	protected $primaryKey = 'idUserClient';
    protected $fillable = ['idUserClient', 'saldo', 'saldoRec'];
}
