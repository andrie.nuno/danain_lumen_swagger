<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListPendanaan extends Model
{

    protected $table = "mitragadai_listpendanaan";
    protected $connection = 'mysql2';
	protected $primaryKey = 'noSbg';
    protected $fillable = [
        'KodeMitra',
        'noSbg',
        'CIF',
        'TglPendanaan',
        'UserName',
        'KTP',
        'TempatLahir',
        'TglLahir',
        'JenKel',
        'Alamat',
        'Kelurahan',
        'Kecamatan',
        'Kota',
        'KodePos',
        'Propinsi',
        'Hp',
        'Produk',
        'JenisAsset',
        'Qty',
        'Gram',
        'Karat',
        'NilaiTaksiran',
        'Rate',
        'NilaiPinjaman',
        'TujuanPinjaman',
        'SectorEkonomi',
        'pict',
        'tgl_proses',
        'isActive',
        'KodePropinsi',
        'KodeCabang',
        'id',
        'Pendidikan',
        'Param_Kota',
        'Param_Propinsi',
        'IbuKandung',
        'NamaPasangan'
    ];
}