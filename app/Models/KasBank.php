<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KasBank extends Model
{
    protected $table = "kasbank";
    protected $connection = 'mysql3';
	protected $primaryKey = 'idKasbank';
    protected $fillable = ['idUserClient', 'tanggal', 'kdTrans', 'amount'];
}
