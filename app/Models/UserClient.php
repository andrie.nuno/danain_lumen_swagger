<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserClient extends Model
{
    protected $table = "tbluserclient";
    protected $connection = 'mysql3';
	protected $primaryKey = 'idUserClient';
    protected $fillable = ['userId', 'userName'];
}
