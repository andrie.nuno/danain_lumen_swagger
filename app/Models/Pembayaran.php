<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{

    protected $table = "pembayaran_pinjaman";
	protected $primaryKey = 'id_pinjaman';
    protected $fillable = [
        'id_penyelenggara',
        'id_pinjaman',
        'id_borrower',
        'id_lender',
        'id_transaksi',
        'id_pembayaran',
        'tgl_jatuh_tempo',
        'tgl_jatuh_tempo_selanjutnya',
        'tgl_pembayaran_borrower',
        'tgl_pembayaran_penyelenggara',
        'sisa_pinjaman_berjalan',
        'id_status_pinjaman',
        'tgl_pelunasan_borrower',
        'tgl_pelunasan_penyelenggara',
        'denda',
        'nilai_pembayaran'
    ];
    public $incrementing = true;
    
    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pinjaman'=>'string',
        'id_borrower'=>'string',
        'id_lender'=>'string',
        'id_transaksi'=>'string',
        'id_pembayaran'=>'string',
    ];

    static function getdata($limit=null){
        $data = Pembayaran::select("*")->limit($limit)->get();
        return $data;
    }
}