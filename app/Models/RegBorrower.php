<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegBorrower extends Model
{
    protected $table = "reg_borrower";
    protected $primaryKey = 'id_pengguna';
    protected $fillable = [
        'id_penyelenggara',
        'id_pengguna', 
        'id_borrower',
        'total_aset',
        'status_kepemilikan_rumah'
    ];

    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pengguna'=>'string',
        'id_borrower'=>'string',
        'status_kepemilikan_rumah'=>'string'
    ];

    static function getdata($limit=null){
        $data = RegBorrower::select("*")->limit($limit)->get();
        return $data;
    }
}
