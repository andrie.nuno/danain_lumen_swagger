<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{

    protected $table = "transaksi_pinjam_meminjam";
	protected $primaryKey = 'id_pinjaman';
    protected $fillable = [
        'id_penyelenggara',
        'id_pinjaman',
        'id_borrower',
        'id_lender',
        'id_transaksi',
        'no_perjanjian_borrower',
        'tgl_perjanjian_borrower',
        'nilai_pendanaan',
        'suku_bunga_pinjaman',
        'satuan_suku_bunga_pinjaman',
        'id_jenis_pembayaran',
        'id_frekuensi_pembayaran',
        'nilai_angsuran',
        'objek_jaminan',
        'jangka_waktu_pinjaman',
        'satuan_jangka_waktu_pinjaman',
        'tgl_jatuh_tempo',
        'tgl_pendanaan',
        'tgl_penyaluran_dana',
        'no_ea_transaksi',
        'frekuensi_pendanaan'
    ];
    public $incrementing = true;
    
    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pinjaman'=>'string',
        'id_borrower'=>'string',
        'id_lender'=>'string',
        'id_transaksi'=>'string',
        'no_perjanjian_borrower'=>'string',
        'objek_jaminan'=>'string',
        'no_ea_transaksi'=>'string',
    ];

    static function getdata($limit=null){
        $data = Transaksi::select("*")->limit($limit)->get();
        return $data;
    }
}