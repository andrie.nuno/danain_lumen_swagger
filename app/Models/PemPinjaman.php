<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemPinjaman extends Model
{

    protected $table = "pengajuan_pemberian_pinjaman";
	protected $primaryKey = 'id_pinjaman';
    protected $fillable = [
        'id_penyelenggara',
        'id_pinjaman',
        'id_borrower',
        'id_lender',
        'no_perjanjian_lender',
        'tgl_perjanjian_lender',
        'tgl_penawaran_pemberian_pinjaman',
        'nilai_penawaran_pinjaman',
        'nilai_penawaran_disetujui',
        'no_va_lender'
    ];
    public $incrementing = true;
    
    protected $casts=[
        'id_penyelenggara'=>'string',
        'id_pinjaman'=>'string',
        'id_borrower'=>'string',
        'id_lender'=>'string',
        'no_perjanjian_lender'=>'string',
        'no_va_lender'=>'string',
    ];

    static function getdata($limit=null){
        $data = PemPinjaman::select("*")->limit($limit)->get();
        return $data;
    }
}