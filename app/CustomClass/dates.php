<?php
namespace App\CustomClass;

class dates{

    public static function bulanInd($date){
        $bulan = array (
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        return $bulan[(int)substr($date, 5, 2)];
    }

    public static function tanggal($angka){
        if ($angka < 10) {
            return '0'.$angka;
        } else {
            return $angka;
        }
    }
}